/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Momo
 */
public class Creneau {
    
    private String id;
    private String debut;
    private String fin;
    
    public Creneau()
    {
        
    }
    
    public Creneau(String id, String debut, String fin)
    {
        this.id = id;
        this.debut = debut;
        this.fin = fin;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public String getDebut()
    {
        return this.debut;
    }
    
    public String getFin()
    {
        return this.fin;
    }
    
    public LocalDateTime getDebutTime()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return LocalDateTime.parse(debut, formatter);
    }
    
    public LocalDateTime getFinTime()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return LocalDateTime.parse(fin, formatter);
    }
    
    public String toString()
    {
        return debut + " " + fin;
    }
}
