/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Momo
 */
public class Court {
    
    private String id;
    private String libelle;
    private String lieu;
    
    public Court()
    {
        
    }
    
    public Court(String id, String libelle, String lieu)
    {
        this.id = id;
        this.libelle = libelle;
        this.lieu = lieu;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public String getLibelle()
    {
        return this.libelle;
    }
    
    public String getLieu()
    {
        return this.lieu;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Court c = (Court) obj;
        return id.equals(c.id);
    }
    
    public String toString()
    {
        return libelle + " à " + lieu;
    }
}
