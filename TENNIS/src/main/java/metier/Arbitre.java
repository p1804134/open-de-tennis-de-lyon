/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Momo
 */
public class Arbitre {
    
    private String id;
    private String nom;
    private String prenom;
    private String type;
    
    public Arbitre()
    {
        
    }
    
    public Arbitre(String id, String nom, String prenom, String type)
    {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.type = type;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Arbitre a = (Arbitre) obj;
        return id.equals(a.id);
    }
    
    public String toString()
    {
        return prenom + " " + nom;
    }
}
