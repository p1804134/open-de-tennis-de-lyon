/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Momo
 */
public class Utilisateur {
    
    private String id;
    private String mdp;
    private String nom;
    private String prenom;
    private String type;
    
    public Utilisateur()
    {
        
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public void setMdp(String mdp)
    {
        this.mdp = mdp;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public String getMdp()
    {
        return this.mdp;
    }
    
    public String toString()
    {
        return id + " " + mdp;
    }
}
