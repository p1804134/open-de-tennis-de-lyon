/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Momo
 */
public class Ramasseur {
    
    private String id;
    private String nom;
    private String prenom;
    
    public Ramasseur()
    {
        
    }
    
    public Ramasseur(String id, String nom, String prenom)
    {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Ramasseur r = (Ramasseur) obj;
        return id.equals(r.id);
    }
    
    public String toString()
    {
        return prenom + " " + nom;
    }
}
