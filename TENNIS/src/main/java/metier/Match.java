/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 *
 * @author Momo
 */
public class Match {
    
    private String id;
    private String op1;
    private String op2;
    private String op3;
    private String op4;
    private String lieu;
    private String id_creneau;
    private String type;
    private String titre;
    private String scoreOp1;
    private String scoreOp2;
    private String arbitreChaise;
    private String arbLn1;
    private String arbLn2;
    private String arbLn3;
    private String arbLn4;
    private String arbLn5;
    private String arbLn6;
    private String arbLn7;
    private String arbLn8;
    private String ram1;
    private String ram2;
    private String ram3;
    private String ram4;
    private String ram5;
    private String ram6;
    private String ram7;
    private String ram8;
    private String ram9;
    private String ram10;
    private String ram11;
    private String ram12;
    
    public Match()
    {
        
    }
    
    public Match(String id, String op1, String op2, String lieu, String creneau)
    {
        this.id = id;
        this.op1 = op1;
        this.op2 = op2;
        this.lieu = lieu;
        this.id_creneau = creneau;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public void setOp1(String op)
    {
        this.op1 = op;
    }
    
    public void setOp2(String op)
    {
        this.op2 = op;
    }
    
    public void setLieu(String lieu)
    {
        this.lieu = lieu;
    }
    
    public void setCreneau(String creneau)
    {
        this.id_creneau = creneau;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public String getOp1()
    {
        return this.op1;
    }
    
    public String getOp2()
    {
        return this.op2;
    }
    
    public String getLieu()
    {
        return this.lieu;
    }
    
    public String getCreneau()
    {
        return this.id_creneau;
    }
    
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Match c = (Match) obj;
        return id.equals(c.id);
    }
     
    public String toString()
    {
        return id + " " + op1 + " " + op2 + " " + lieu + " " + id_creneau;
    }
}
