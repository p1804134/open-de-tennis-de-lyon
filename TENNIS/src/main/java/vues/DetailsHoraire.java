/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vues;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import metier.Arbitre;
import metier.Court;
import metier.Creneau;
import metier.Match;
import metier.Ramasseur;

/**
 *
 * @author Momo
 */
public class DetailsHoraire extends javax.swing.JFrame {
    Fenêtre par;
    Manager man;
    DefaultListModel listMatchs;
    DefaultListModel listArbitres;
    DefaultListModel listRamasseurs;

    DefaultComboBoxModel listArbitresChaise;
    DefaultComboBoxModel listCourts;
    DefaultComboBoxModel listType;

    JCheckBox j1;
    String idC;
    
    int startM = 5;
    
    public static int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        for (int i=0; i < ret.length; i++)
        {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }
    
    /**
     * Creates new form DetailsMatch
     */
    public DetailsHoraire(Fenêtre par, Manager man, String id) {
        this.idC = id;
        this.man = man;
        this.par = par;
        listMatchs = new DefaultListModel();
        listMatchs.clear();
        ArrayList<String> listeMatchs = new ArrayList<String>();
        for (Match m : par.mDAO.getMatchs())
        {
            listeMatchs.add(m.getId() + " : " + par.jDAO.getJoueur(m.getOp1()) + " -- " + par.jDAO.getJoueur(m.getOp2()));
        }
        listMatchs.addAll(listeMatchs);
        
        listRamasseurs = new DefaultListModel();
        listRamasseurs.removeAllElements();
        ArrayList<String> listeRamasseurs = new ArrayList<String>();
        for (Ramasseur r : par.rDAO.getRamasseurs())
        {
            listeRamasseurs.add(r.toString());
        }
        listRamasseurs.addAll(listeRamasseurs);
        
        listArbitresChaise = new DefaultComboBoxModel();
        listArbitresChaise.removeAllElements();
        ArrayList<String> listeArbitres = new ArrayList<String>();
        for (Arbitre a : par.aDAO.getArbitresChaise())
        {
            listeArbitres.add(a.toString());
        }
        listArbitresChaise.addAll(listeArbitres);
        
        listArbitres = new DefaultListModel();
        listArbitres.removeAllElements();
        listeArbitres = new ArrayList<String>();
        for (Arbitre a : par.aDAO.getArbitresLigne())
        {
            listeArbitres.add(a.toString());
        }
        listArbitres.addAll(listeArbitres);
        
        listCourts = new DefaultComboBoxModel();
        listCourts.removeAllElements();
        ArrayList<String> listeCourts = new ArrayList<String>();
        for (Court a : par.cDAO.getCourts())
        {
            listeCourts.add(a.toString());
        }
        listCourts.addAll(listeCourts);
        
        String[] cars = {"", "SELECTION", "TOURNOI", "FINALE"};
        listType = new DefaultComboBoxModel(cars);
                
        initComponents();
        
        ArrayList<Match> mC = par.mDAO.getMatchsByCreneau(par.crDAO.getCreneau(id));
        if(mC.size()!=0) 
        {
            Match m = mC.get(0);
            UpdateDetails(m);
        }
    }
    
    void UpdateDetails(Match m)
    {
        jComboBox2.setSelectedIndex( par.cDAO.getCourts().indexOf( par.cDAO.getCourt(m.getLieu()) ) );
        jComboBox1.setSelectedIndex( par.aDAO.getArbitresChaise().indexOf( par.aDAO.getArbitre(par.mDAO.getArbitreChaise(m)) ) );
        jComboBox3.setSelectedIndex( Integer.parseInt(par.mDAO.getType(m))-1 );
        jList1.setSelectedIndex( par.mDAO.getMatchs().indexOf( par.mDAO.getMatch(m.getId()) ) );
        
        ArrayList<Integer> ram = new ArrayList<Integer>();
        
        for (Iterator <String> iterator = par.mDAO.getRamasseursId(m).iterator(); iterator.hasNext();) {
            String next = iterator.next();
            ram.add(par.rDAO.getRamasseurs().indexOf(par.rDAO.getRamasseur(next)));
        }
        
        jList2.setSelectedIndices( convertIntegers(ram) );
        jList2.revalidate();
        
        
        ArrayList<Integer> arbLigne = new ArrayList<Integer>();
        
        for (Iterator <String> iterator = par.mDAO.getArbitresLigneId(m).iterator(); iterator.hasNext();) {
            String next = iterator.next();
            System.out.println(next);
            arbLigne.add(par.aDAO.getArbitres().indexOf(par.aDAO.getArbitre(next)));
        }
        
        jList3.setSelectedIndices( convertIntegers(arbLigne) );
        jList3.revalidate();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("joueur 1 -- joueur 2");
        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jList1.setModel(listMatchs);
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("date et heure");
        jLabel3.setText("Le " + Integer.toString(par.crDAO.getCreneau(idC).getDebutTime().getDayOfMonth()) + "/" + par.crDAO.getCreneau(idC).getDebutTime().getMonthValue() + " à " + Integer.toString(par.crDAO.getCreneau(idC).getDebutTime().getHour()) + " H.");
        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jComboBox1.setModel(listArbitresChaise);
        jComboBox1.setName(""); // NOI18N
        jComboBox1.setToolTipText("");
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel4.setText("arbitre de chaise :");

        jComboBox2.setModel(listCourts);
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jLabel5.setText("court :");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Titre du match :");
        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel6.setText("arbitres de ligne :");

        jLabel7.setText("ramasseurs de balle :");

        jButton1.setText("Enregistrer");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jList2.setModel(listRamasseurs);
        jScrollPane4.setViewportView(jList2);

        jList3.setModel(listArbitres);
        jScrollPane5.setViewportView(jList3);

        jComboBox3.setModel(listType);
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel8.setText("type de match :");

        jLabel9.setText("Changer le match pour cet horraire :");

        jButton2.setText("Annuler");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane1)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(96, 96, 96)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(49, 49, 49)
                                        .addComponent(jLabel4)))
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(144, 144, 144))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                        .addGap(19, 19, 19)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(jScrollPane5))))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        /*TableCellRenderer renderer = el. getCellRenderer(x, y);
        
        Component component = el.prepareRenderer(renderer, x, y);
        component.setBackground(Color.RED);*/
        
        if (jList1.getSelectedIndex()==-1) return; //erreur
        Match m;
        ArrayList<Match> lM = par.mDAO.getMatchsByCreneau(par.crDAO.getCreneau(idC));
        if (lM.size()>0)
        {
            if (!lM.get(0).equals(par.mDAO.getMatchs().get(jList1.getSelectedIndex())))
            {
                m = lM.get(0);
                ArrayList<String> ids = new ArrayList<String>();
                for (int i = 0; i < 8; i++) {
                    ids.add("0");
                }
                par.mDAO.updateArbitresLigne(m, ids);
                ids = new ArrayList<String>();
                for (int i = 0; i < 12; i++) {
                    ids.add("0");
                }
                par.mDAO.updateRamasseurs(m, ids);
                par.mDAO.updateArbitreChaise(m, "0");
                par.mDAO.updateCreneau(m, new Creneau("0", "0", "0"));
                par.mDAO.updateLieu(m, new Court("0", "0", "0"));
            }
        }
        
        m = par.mDAO.getMatchs().get(jList1.getSelectedIndex());
        par.mDAO.updateCreneau(m, par.crDAO.getCreneau(idC));
        
        if (jComboBox2.getSelectedIndex()!=-1)
            par.mDAO.updateLieu(m, par.cDAO.getCourts().get(jComboBox2.getSelectedIndex()));
        else
            par.mDAO.updateLieu(m, new Court("0", "0", "0"));

        if (jComboBox1.getSelectedIndex()!=-1)
            par.mDAO.updateArbitreChaise(m, par.aDAO.getArbitres().get(jComboBox1.getSelectedIndex()).getId());
        else
            par.mDAO.updateArbitreChaise(m, "0");

        if (jComboBox3.getSelectedIndex()!=-1)
        {
            par.mDAO.updateType(m, Integer.toString(jComboBox3.getSelectedIndex()+1));
        }
        else
            par.mDAO.updateType(m, "0");
        
        if (jList3.getSelectedIndices().length!=0)
        {
            ArrayList<String> ids = new ArrayList<String>();
            ArrayList<Arbitre> arb = (ArrayList<Arbitre>) par.aDAO.getArbitresLigne();
            
            for (int i = 0; i < jList3.getSelectedIndices().length && i<8; i++) {
                ids.add(arb.get(jList3.getSelectedIndices()[i]).getId());
            }
            par.mDAO.updateArbitresLigne(m, ids);
        }
        else
        {
            ArrayList<String> ids = new ArrayList<String>();
            for (int i = 0; i < 8; i++) {
                ids.add("0");
            }
            par.mDAO.updateArbitresLigne(m, ids);
        }
        
        if (jList2.getSelectedIndices().length!=0)
        {
            ArrayList<String> ids = new ArrayList<String>();
            ArrayList<Ramasseur> rams = (ArrayList<Ramasseur>) par.rDAO.getRamasseurs();
            
            for (int i = 0; i < jList2.getSelectedIndices().length && i<12; i++) {
                ids.add(rams.get(jList2.getSelectedIndices()[i]).getId());
            }
            par.mDAO.updateRamasseurs(m, ids);
        }
        else
        {
            ArrayList<String> ids = new ArrayList<String>();
            for (int i = 0; i < 12; i++) {
                ids.add("0");
            }
            par.mDAO.updateRamasseurs(m, ids);
        }

        
        man.UpdateMatchs();
        man.UpdateDate();
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        jLabel1.setText(jList1.getSelectedValue());
        UpdateDetails(par.mDAO.getMatchs().get(jList1.getSelectedIndex()));
    }//GEN-LAST:event_jList1ValueChanged

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JList<String> jList3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    // End of variables declaration//GEN-END:variables
}
