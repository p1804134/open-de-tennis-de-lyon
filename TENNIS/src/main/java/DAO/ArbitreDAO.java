/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import metier.Arbitre;

/**
 *
 * @author Momo
 */
public class ArbitreDAO {
    private Connection connectionBD;
    
    public List<Arbitre> getArbitres() {
        ArrayList<Arbitre> listA = new ArrayList<Arbitre>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from ARBITRE");
            while ( rset.next()) {
                Arbitre uTemp = new Arbitre(Integer.toString(rset.getInt("ID_ARBITRE")), rset.getString("NOM"), rset.getString("PRENOM"), Integer.toString(rset.getInt("TYPE")));
                listA.add(uTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listA;
    }
    
    public List<Arbitre> getArbitresChaise() {
        ArrayList<Arbitre> listA = new ArrayList<Arbitre>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from ARBITRE WHERE TYPE=1");
            while ( rset.next()) {
                Arbitre uTemp = new Arbitre(Integer.toString(rset.getInt("ID_ARBITRE")), rset.getString("NOM"), rset.getString("PRENOM"), Integer.toString(rset.getInt("TYPE")));
                listA.add(uTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listA;
    }
    
    public List<Arbitre> getArbitresLigne() {
        ArrayList<Arbitre> listA = new ArrayList<Arbitre>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from ARBITRE WHERE TYPE=2");
            while ( rset.next()) {
                Arbitre uTemp = new Arbitre(Integer.toString(rset.getInt("ID_ARBITRE")), rset.getString("NOM"), rset.getString("PRENOM"), Integer.toString(rset.getInt("TYPE")));
                listA.add(uTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listA;
    }
    
    public Arbitre getArbitre(String id) {
        Arbitre arbitre = new Arbitre();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from ARBITRE WHERE ID_ARBITRE = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Arbitre aTemp = new Arbitre(Integer.toString(rset.getInt("ID_ARBITRE")), rset.getString("NOM"), rset.getString("PRENOM"), Integer.toString(rset.getInt("TYPE")));
            arbitre = aTemp;
        } catch (SQLException ex) {
            Logger.getLogger(ArbitreDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arbitre;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}