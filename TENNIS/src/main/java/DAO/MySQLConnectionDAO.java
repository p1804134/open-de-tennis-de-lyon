/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.mysql.cj.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Momo
 */
public class MySQLConnectionDAO {
    private MySQLConnectionDAO(){
        
    }
    
    private static Connection conn;
    
    public static Connection getConnection() throws SQLException
    {
        if (conn!=null && conn.isValid(0))
            return conn;
        
        Driver pilote ;
        try {
            pilote = new Driver();
            DriverManager.registerDriver(pilote);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLConnectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String protocole =  "jdbc:mysql:" ;
        String ip =  "iutdoua-web.univ-lyon1.fr" ;
        String port =  "3306" ;
        String nomBase =  "p1805931" ;
        String conString = protocole +  "//" + ip +  ":" + port +  "/" + nomBase ;
        String nomConnexion =  "p1805931" ;
        String motDePasse =  "368907" ;  // dépend du contexte
        
        conn = DriverManager.getConnection(conString, nomConnexion, motDePasse);
        
        return conn;
    }
    
}