/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.Creneau;
import vues.Fenêtre;

/**
 *
 * @author Momo
 */
public class CreneauDAO {
    private Connection connectionBD;
    
    public List<Creneau> getCreneaus() {
        ArrayList<Creneau> listC = new ArrayList<Creneau>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from CRENEAU");
            while ( rset.next()) {
                Creneau cTemp = new Creneau(rset.getString("ID_CRENEAU"), rset.getString("DATE_DEBUT"), rset.getString("DATE_FIN"));
                listC.add(cTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreneauDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listC;
    }
    
    public Creneau getCreneau(String id) {
        Creneau creneau = new Creneau();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from CRENEAU WHERE ID_CRENEAU = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Creneau cTemp = new Creneau(rset.getString("ID_CRENEAU"), rset.getString("DATE_DEBUT"), rset.getString("DATE_FIN"));
            creneau = cTemp;
        } catch (SQLException ex) {
            Logger.getLogger(CreneauDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return creneau;
    }
    
    public Creneau getCreneauByDebut(LocalDateTime d) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        
        ArrayList<Creneau> listC = (ArrayList<Creneau>)getCreneaus();
        for (Iterator<Creneau> iterator = listC.iterator(); iterator.hasNext();) {
            Creneau next = iterator.next();
            LocalDateTime dateC = LocalDateTime.parse(next.getDebut(), formatter);
            if ( dateC.isEqual(d) )
                return next;
        }
        
        return null;
    }
    
    public List<Creneau> getCreneauxWeek(LocalDateTime d) {
        ArrayList<Creneau> s = new ArrayList<Creneau>();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        
        ArrayList<Creneau> listC = (ArrayList<Creneau>)getCreneaus();
        for (Iterator<Creneau> iterator = listC.iterator(); iterator.hasNext();) {
            Creneau next = iterator.next();
            LocalDateTime dateC = LocalDateTime.parse(next.getDebut(), formatter);
            if ( dateC.with(TemporalAdjusters.previous( DayOfWeek.MONDAY )).toLocalDate().isEqual(d.with(TemporalAdjusters.previous( DayOfWeek.MONDAY )).toLocalDate()) ) s.add(next);
        }
        
        return s;
    }
    
    public void creerCreneau(Creneau c)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("INSERT INTO CRENEAU (DATE_DEBUT, DATE_FIN) VALUES (?, ?)");
            stmt.setString(1, c.getDebut());
            stmt.setString(2, c.getFin());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}