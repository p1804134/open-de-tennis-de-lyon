/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.Court;

/**
 *
 * @author Momo
 */
public class CourtDAO {
    private Connection connectionBD;
    
    public List<Court> getCourts() {
        ArrayList<Court> listC = new ArrayList<Court>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from COURT");
            while ( rset.next()) {
                Court jTemp = new Court(rset.getString("ID_COURT"), rset.getString("LIBELLE_COURT"), rset.getString("LIEU"));
                listC.add(jTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourtDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listC;
    }
    
    public Court getCourt(String id) {
        Court court = new Court();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from COURT WHERE ID_COURT = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Court jTemp = new Court(rset.getString("ID_COURT"), rset.getString("LIBELLE_COURT"), rset.getString("LIEU"));
            court = jTemp;
        } catch (SQLException ex) {
            Logger.getLogger(CourtDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return court;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}