/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.Joueur;

/**
 *
 * @author Momo
 */
public class JoueurDAO {
    private Connection connectionBD;
    
    public List<Joueur> getJoueurs() {
        ArrayList<Joueur> listJ = new ArrayList<Joueur>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from JOUEUR");
            while ( rset.next()) {
                Joueur jTemp = new Joueur(rset.getString("ID_JOUEUR"), rset.getString("NOM"), rset.getString("PRENOM"));
                listJ.add(jTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(JoueurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listJ;
    }
    
    public Joueur getJoueur(String id) {
        Joueur joueur = new Joueur();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from JOUEUR WHERE ID_JOUEUR = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Joueur jTemp = new Joueur(rset.getString("ID_JOUEUR"), rset.getString("NOM"), rset.getString("PRENOM"));
            joueur = jTemp;
        } catch (SQLException ex) {
            Logger.getLogger(JoueurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return joueur;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}