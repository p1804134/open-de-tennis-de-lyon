/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.Court;
import metier.Creneau;
import metier.Match;

/**
 *
 * @author Momo
 */
public class MatchDAO {
    private Connection connectionBD;
    
    public List<Match> getMatchs() {
        ArrayList<Match> listM = new ArrayList<Match>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from MATCHS");
            while ( rset.next()) {
                Match mTemp = new Match(Integer.toString(rset.getInt("ID_MATCH")), Integer.toString(rset.getInt("OP1")), Integer.toString(rset.getInt("OP2")), Integer.toString(rset.getInt("LIEU")), Integer.toString(rset.getInt("ID_CRENEAU")));
                listM.add(mTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listM;
    }
    
    public ArrayList<Match> getMatchsByCreneau(Creneau c) {
        ArrayList<Match> matchs = (ArrayList)getMatchs();
        ArrayList<Match> s = new ArrayList<Match>();
        
        for (Iterator<Match> iterator = matchs.iterator(); iterator.hasNext();) {
            Match next = iterator.next();
            if (next.getCreneau().equals(c.getId()))
            {
                s.add(next);
            }
        }
        
        return s;
    }
    
    public Match getMatch(String id) {
        Match match = new Match();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from MATCHS WHERE ID_MATCH = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Match mTemp = new Match(Integer.toString(rset.getInt("ID_MATCH")), Integer.toString(rset.getInt("OP1")), Integer.toString(rset.getInt("OP2")), Integer.toString(rset.getInt("LIEU")), Integer.toString(rset.getInt("ID_CRENEAU")));
            match = mTemp;
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return match;
    }
    
    public void creerMatch(String op1, String op2, String lieu, String creneau)
    {
        Match mTemp = new Match("null", op1, op2, lieu, creneau);
        creerMatch(mTemp);
    }
    
    public void creerMatch(Match m)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("INSERT INTO MATCHS (OP1, OP2) VALUES (?, ?)");
            stmt.setInt(1, Integer.parseInt(m.getOp1()));
            stmt.setInt(2, Integer.parseInt(m.getOp2()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateLieu(Match m, Court c)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET LIEU=? WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(c.getId()));
            stmt.setInt(2, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateCreneau(Match m, Creneau c)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET ID_CRENEAU=? WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(c.getId()));
            stmt.setInt(2, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateType(Match m, String type)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET TYPE=? WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(type));
            stmt.setInt(2, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateTitre(Match m, String titre)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET TITRE=? WHERE ID_MATCH=?");
            stmt.setString(1, titre);
            stmt.setInt(2, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateScore(Match m, String op1, String op2)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET SCOREOP1=?, SCOREOP2=? WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(op1));
            stmt.setInt(2, Integer.parseInt(op2));
            stmt.setInt(3, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateArbitreChaise(Match m, String id)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("UPDATE MATCHS SET ARBITRE_CHAISE=? WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(id));
            stmt.setInt(2, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateArbitresLigne(Match m, ArrayList<String> ids)
    {
        if (ids.size()!=8) return;
        try {
            String req = "UPDATE MATCHS SET ARB_LN_1=?";
            for (int i = 0; i < 7; i++) {
                req += ", ARB_LN_"+Integer.toString(i+2)+"=?";
            }
            req +=  " WHERE ID_MATCH=?";
            PreparedStatement stmt = connectionBD.prepareStatement(req);
            for (int i = 0; i < 8; i++) {
                stmt.setInt(i+1, Integer.parseInt(ids.get(i)));
            }
            stmt.setInt(9, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateRamasseurs(Match m, ArrayList<String> ids)
    {
        if (ids.size()!=12) return;
        try {
            String req = "UPDATE MATCHS SET RAM_1=?";
            for (int i = 0; i < 11; i++) {
                req += ", RAM_"+Integer.toString(i+2)+"=?";
            }
            req +=  " WHERE ID_MATCH=?";
            PreparedStatement stmt = connectionBD.prepareStatement(req);
            for (int i = 0; i < 12; i++) {
                stmt.setInt(i+1, Integer.parseInt(ids.get(i)));
            }
            stmt.setInt(13, Integer.parseInt(m.getId()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<String> getRamasseursId(Match m)
    {
        ArrayList<String> ids = new ArrayList<String>();
        
        try {
            String req = "SELECT RAM_1";
            for (int i = 0; i < 11; i++) {
                req += ", RAM_"+Integer.toString(i+2);
            }
            req +=  " FROM MATCHS WHERE ID_MATCH=?";
            PreparedStatement stmt = connectionBD.prepareStatement(req);
            stmt.setInt(1, Integer.parseInt(m.getId()));
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                for (int j = 0; j < 12; j++) {
                    ids.add(Integer.toString(rset.getInt("RAM_" + Integer.toString(j+1) )));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ids;
    }
    
    public ArrayList<String> getArbitresLigneId(Match m)
    {
        ArrayList<String> ids = new ArrayList<String>();
        
        try {
            String req = "SELECT ARB_LN_1";
            for (int i = 0; i < 7; i++) {
                req += ", ARB_LN_"+Integer.toString(i+2);
            }
            req +=  " FROM MATCHS WHERE ID_MATCH=?";
            PreparedStatement stmt = connectionBD.prepareStatement(req);
            stmt.setInt(1, Integer.parseInt(m.getId()));
            System.out.println(stmt);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                for (int j = 0; j < 8; j++) {
                    ids.add(Integer.toString(rset.getInt("ARB_LN_" + Integer.toString(j+1) )));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ids;
    }
    
    public String getArbitreChaise(Match m)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("SELECT ARBITRE_CHAISE FROM MATCHS WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(m.getId()));
            System.out.println(stmt);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                return Integer.toString(rset.getInt("ARBITRE_CHAISE"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getType(Match m)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("SELECT TYPE FROM MATCHS WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(m.getId()));
            System.out.println(stmt);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                return Integer.toString(rset.getInt("TYPE"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getScoreOp1(Match m)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("SELECT SCOREOP1 FROM MATCHS WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(m.getId()));
            System.out.println(stmt);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                return Integer.toString(rset.getInt("SCOREOP1"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getScoreOp2(Match m)
    {
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("SELECT SCOREOP2 FROM MATCHS WHERE ID_MATCH=?");
            stmt.setInt(1, Integer.parseInt(m.getId()));
            System.out.println(stmt);
            ResultSet rset = stmt.executeQuery();
            if(rset.next()) {
                return Integer.toString(rset.getInt("SCOREOP2"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MatchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}