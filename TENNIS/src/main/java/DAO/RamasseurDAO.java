/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import metier.Ramasseur;

/**
 *
 * @author Momo
 */
public class RamasseurDAO {
    private Connection connectionBD;
    
    public List<Ramasseur> getRamasseurs() {
        ArrayList<Ramasseur> listR = new ArrayList<Ramasseur>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from RAMASSEUR");
            while ( rset.next()) {
                Ramasseur rTemp = new Ramasseur(Integer.toString(rset.getInt("ID_RAMASSEUR")), rset.getString("NOM"), rset.getString("PRENOM"));
                listR.add(rTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RamasseurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listR;
    }
    
    public Ramasseur getRamasseur(String id) {
        Ramasseur ramasseur = new Ramasseur();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from RAMASSEUR WHERE ID_RAMASSEUR = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Ramasseur rTemp = new Ramasseur(Integer.toString(rset.getInt("ID_RAMASSEUR")), rset.getString("NOM"), rset.getString("PRENOM"));
            ramasseur = rTemp;
        } catch (SQLException ex) {
            Logger.getLogger(RamasseurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ramasseur;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}