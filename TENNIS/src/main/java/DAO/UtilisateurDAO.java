/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import metier.Utilisateur;

/**
 *
 * @author Momo
 */
public class UtilisateurDAO {
    private Connection connectionBD;
    
    public List<Utilisateur> getUtilisateurs() {
        ArrayList<Utilisateur> listMB = new ArrayList<Utilisateur>();
        try {
            Statement stmt = connectionBD.createStatement();
            ResultSet rset = stmt.executeQuery ("select * from UTILISATEUR");
            while ( rset.next()) {
                Utilisateur uTemp = new Utilisateur();
                uTemp.setId(rset.getString("ID_UTILISATEUR"));
                uTemp.setMdp(rset.getString("MDP"));
                listMB.add(uTemp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listMB;
    }
    
    public Utilisateur getUtilisateur(String id) {
        Utilisateur utilisateur = new Utilisateur();
        try {
            PreparedStatement stmt = connectionBD.prepareStatement("select * from UTILISATEUR WHERE ID_UTILISATEUR = ?");
            stmt.setString(1, id);
            ResultSet rset = stmt.executeQuery();
            if(!rset.next())return null;
            Utilisateur uTemp = new Utilisateur();
            uTemp.setId(rset.getString("ID_UTILISATEUR"));
            uTemp.setMdp(rset.getString("MDP"));
            utilisateur = uTemp;
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return utilisateur;
    }

    public void setConnection(Connection c) {
        this.connectionBD = c;
    }
}