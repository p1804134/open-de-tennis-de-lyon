<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'Utilisateur.php');
class UtilisateurDAO extends DAO{

    public function getUtilisateurs(){
            $res = $this -> queryAll('SELECT * FROM UTILISATEUR');
            $list_utilisateur = array();
        if($res){
            
            foreach($res as $temp){
                $list_utilisateur[$temp['ID_UTILISATEUR']] = new Utilisateur($temp['NUM_UTILISATEUR'], $temp['ID_UTILISATEUR'], $temp['MDP'], $temp['NOM'], $temp['PRENOM'], $temp['TYPE']);
            }
        }
        return $list_utilisateur;
    }

    public function getUtilisateurById($idUser){
        $res = $this -> queryRow('SELECT * FROM UTILISATEUR where ID_UTILISATEUR = ?', array($idUser));
        if($res){  
            $utilisateur = new Utilisateur($res['NUM_UTILISATEUR'], $res['ID_UTILISATEUR'], $res['MDP'], $res['NOM'], $res['PRENOM'], $res['TYPE']);
        }
        else 
            $utilisateur =null;
        return $utilisateur;
    }

    public function createUtilisateur($utilisateur){
        $req = $this -> queryInsert('INSERT INTO UTILISATEUR(ID_UTILISATEUR, MDP, NOM, PRENOM, TYPE) VALUES(?,?,?,?,?)',  [$utilisateur->getIdUtilisateur(), $utilisateur->getMdp(), $utilisateur->getNom(), $utilisateur->getPrenom(),$utilisateur->getType()]);
        return ($req && is_null($this->getErreur()));
    }

    public function changeUtilisateur($utilisateur, $idUser){
        $req = $this -> queryInsert('UPDATE UTILISATEUR SET ID_UTILISATEUR = ?, NOM = ?, PRENOM = ?, TYPE = ? WHERE ID_UTILISATEUR = ? ',  [$utilisateur->getIdUtilisateur(), $utilisateur->getNom(), $utilisateur->getPrenom(),$utilisateur->getType(), $idUser]);
        return ($req && is_null($this->getErreur()));
    }
    public function ChangeUtilisateurType3($idUser){
        $req = $this -> queryInsert('UPDATE UTILISATEUR SET TYPE = "3" where ID_UTILISATEUR = ?', array($idUser));
        return ($req && is_null($this->getErreur()));
    }
    public function ChangeUtilisateurType2($idUser){
        $req = $this -> queryInsert('UPDATE UTILISATEUR SET TYPE = "2" where ID_UTILISATEUR = ?', array($idUser));
        return ($req && is_null($this->getErreur()));
    }
    public function supprUtilisateurById($idUser){
        $req = $this -> queryInsert('DELETE FROM UTILISATEUR WHERE ID_UTILISATEUR = ?', array($idUser));
        return ($req && is_null($this->getErreur()));
    }
    public function changeMdp($idUser, $newmdp){
        $req = $this -> queryInsert('UPDATE UTILISATEUR SET MDP = ? where ID_UTILISATEUR = ?', array($newmdp, $idUser));
        return ($req && is_null($this->getErreur()));
    }

}
