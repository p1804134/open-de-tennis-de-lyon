<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'Hebergement.php');
class HebergementDAO extends DAO{

    public function getHebergement(){
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT');
            $list_hebergement = array();
        if($res){
            
            foreach($res as $temp){
                $list_hebergement[$temp['ID_HEBERG']] = new Hebergement($temp['ID_HEBERG'], $temp['NOM'], $temp['TYPE'], $temp['ID_GERANT'], $temp['LIEUX'], $temp['NB_ETOILES'], $temp['NB_CHAMBRES']);
            }
        }
        return $list_hebergement;                
    }

    public function getHebergementById($IdUser){
        $res = $this -> queryRow('SELECT * FROM HEBERGEMENT WHERE ID_GERANT = ?', array($IdUser));
        if($res){
            return new Hebergement($res['ID_HEBERG'], $res['NOM'], $res['TYPE'], $res['ID_GERANT'], $res['LIEUX'], $res['NB_ETOILES'], $res['NB_CHAMBRES']);
        }
        else return null;
    }

    public function getHebergementByIdHeberg($IdHeberg){
        $res = $this -> queryRow('SELECT * FROM HEBERGEMENT WHERE ID_HEBERG = ?', array($IdHeberg));
        if($res){
            return new Hebergement($res['ID_HEBERG'], $res['NOM'], $res['TYPE'], $res['ID_GERANT'], $res['LIEUX'], $res['NB_ETOILES'], $res['NB_CHAMBRES']);
        }
        else return null;
    }

    public function getHebergementTrie($type, $nbEtoile, $nbChambre){
        if ($type!= "---" and $nbEtoile!="---" and $nbChambre!="---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE TYPE = ? and NB_ETOILES = ? and NB_CHAMBRES >= ?' , array($type, $nbEtoile, $nbChambre));
        else if ($type!= "---" and $nbEtoile!="---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE TYPE = ? and NB_ETOILES = ?' , array($type, $nbEtoile));
        else if ($type!= "---" and $nbChambre!="---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE TYPE = ? and NB_CHAMBRES >= ?' , array($type, $nbChambre)); 
        else if ($nbEtoile!= "---" and $nbChambre!="---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE NB_ETOILES = ? and NB_CHAMBRES >= ?' , array($nbEtoile, $nbChambre)); 
        else if ($type!= "---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE TYPE = ?', array($type));       
        else if ($nbChambre!= "---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE NB_CHAMBRES >= ?', array($nbChambre));
        else if ($nbEtoile!= "---")
            $res = $this -> queryAll('SELECT * FROM HEBERGEMENT WHERE NB_ETOILES = ?', array($nbEtoile));

        $list_hebergement = array();
        if($res){
            
            foreach($res as $temp){
                $list_hebergement[$temp['ID_HEBERG']] = new Hebergement($temp['ID_HEBERG'], $temp['NOM'], $temp['TYPE'], $temp['ID_GERANT'], $temp['LIEUX'], $temp['NB_ETOILES'], $temp['NB_CHAMBRES']);
            }
        }
        return $list_hebergement;
    }   

    public function createHebergement($hebergement){
        $req = $this -> queryInsert('INSERT INTO HEBERGEMENT(NOM, TYPE, ID_GERANT, LIEUX, NB_ETOILES, NB_CHAMBRES) VALUE(?,?,?,?,?,?)',[$hebergement->getNom(), $hebergement->getType(), $hebergement->getIdGerant(), $hebergement->getLieux(), $hebergement->getNbEtoiles(), $hebergement->getNbChambres()]); 
            return ($req && is_null($this->getErreur()));
    }

    public function changeHebergement($hebergement, $idHebergement){
        $req = $this -> queryInsert('UPDATE HEBERGEMENT SET NOM = ?, TYPE = ?, LIEUX = ?, NB_ETOILES = ?, NB_CHAMBRES = ? where ID_HEBERG = ?',[$hebergement->getNom(), $hebergement->getType(), $hebergement->getLieux(), $hebergement->getNbEtoiles(), $hebergement->getNbChambres(), $idHebergement]); 
            return ($req && is_null($this->getErreur()));
    }

    public function changeHebergementMail($idGerant, $idHebergement){
        $req = $this -> queryInsert('UPDATE HEBERGEMENT SET ID_GERANT = ? where ID_HEBERG = ?',[$idGerant, $idHebergement]); 
            return ($req && is_null($this->getErreur()));
    }

    public function supprHebergementById($idHeberg){
        $req = $this -> queryInsert('DELETE FROM HEBERGEMENT WHERE ID_HEBERG = ?', array($idHeberg));
        return ($req && is_null($this->getErreur()));
    }
}