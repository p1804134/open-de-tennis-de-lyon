<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'Reservation.php');
class ReservationDAO extends DAO{

    public function ajouterReservation($reservation){
        $req = $this -> queryInsert('INSERT INTO RESERVATION (ID_HEBERG, NOM, PRENOM, ADRESSE_MAIL, TYPE, DATE_DEBUT, DATE_FIN, NB_RESERVATION) VALUES(?,?,?,?,?,?,?,?)',  [$reservation->getIdHeberg(),$reservation->getNom(),$reservation->getPrenom(),$reservation->getMail(),$reservation->getType(),"2020-05-".$reservation->getDebut(),"2020-05-".$reservation->getFin(),$reservation->getNbReserv()]);
        return ($req && is_null($this->getErreur()));
    }

    public function modifierReservation($reservation){
        $req = $this -> queryInsert('UPDATE RESERVATION SET NOM = ?, PRENOM = ?, ADRESSE_MAIL = ?, DATE_DEBUT = ?, DATE_FIN = ?, NB_RESERVATION = ? where NUM_RESERVATION = ?',  [$reservation->getNom(),$reservation->getPrenom(),$reservation->getMail(),"2020-05-".$reservation->getDebut(),"2020-05-".$reservation->getFin(),$reservation->getNbReserv(),$reservation->getNumReserv()]);
        return ($req && is_null($this->getErreur()));
    }

    public function supprimerReservation($reservation){
        $req = $this -> queryInsert('DELETE FROM RESERVATION WHERE NUM_RESERVATION = ? ', [$reservation]);
        return ($req && is_null($this->getErreur()));
    }

    public function getReservation($idHeberg){
        $res = $this -> queryAll('SELECT * FROM RESERVATION WHERE ID_HEBERG = ?', [$idHeberg]);
            $list_reservation = array();
        if($res){
            
            foreach($res as $temp){
                $list_reservation[$temp['NUM_RESERVATION']] = new Reservation($temp['NUM_RESERVATION'], $temp['ID_HEBERG'], $temp['NOM'], $temp['PRENOM'], $temp['ADRESSE_MAIL'], $temp['TYPE'], $temp['DATE_DEBUT'], $temp['DATE_FIN'], $temp['NB_RESERVATION']);
            }
        }
        return $list_reservation;
    }

    public function getReservationbyNum($numReserv){
        $res = $this -> queryRow('SELECT * FROM RESERVATION WHERE NUM_RESERVATION = ?', [$numReserv]);
            
        if($res){
            return new Reservation($res['NUM_RESERVATION'], $res['ID_HEBERG'], $res['NOM'], $res['PRENOM'], $res['ADRESSE_MAIL'], $res['TYPE'], $res['DATE_DEBUT'], $res['DATE_FIN'], $res['NB_RESERVATION']);
        }
        return null;
    }

    public function getReservations(){
        $res = $this -> queryAll('SELECT * FROM RESERVATION');
            $list_reservation = array();
        if($res){
            
            foreach($res as $temp){
                $list_reservation[$temp['NUM_RESERVATION']] = new Reservation($temp['NUM_RESERVATION'], $temp['ID_HEBERG'], $temp['NOM'], $temp['PRENOM'], $temp['ADRESSE_MAIL'], $temp['TYPE'], $temp['DATE_DEBUT'], $temp['DATE_FIN'], $temp['NB_RESERVATION']);
            }
        }
        return $list_reservation;
    }
}