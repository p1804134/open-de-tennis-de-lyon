<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'Planning.php');
class PlanningDAO extends DAO{

    public function getPlanning($idHeberg){
        $res = $this -> queryAll('SELECT * FROM PLANNING WHERE ID_HEBERG = ?', [$idHeberg]);
        $list_planning = array();
    if($res){
        
        foreach($res as $temp){
            $list_planning[$temp['DATE']] = new Planning($temp['ID_HEBERG'], $temp['DATE'], $temp['NB_CHAMBRES_DISPO']);
        }
    }
    return $list_planning;     
}     
    
    public function createPlanningForHebergement($idHeberg){
        for ($i=15; $i<24 ;$i++){
            $req = $this -> queryInsert('INSERT INTO PLANNING(ID_HEBERG, DATE, NB_CHAMBRES_DISPO) VALUE(?,?,?)', [$idHeberg, "2020-05-".$i, 0]); 
        }
        return ($req && is_null($this->getErreur()));
    }

    public function modifPlanningForHebergement($idHeberg, $date, $nbChambre){        
        $req = $this -> queryInsert('UPDATE PLANNING SET NB_CHAMBRES_DISPO = ? where ID_HEBERG = ? and DATE = ?', [$nbChambre, $idHeberg, $date]); 
            return ($req && is_null($this->getErreur()));
    }

    public function modifNbChambres($idHeberg, $dateFin, $dateDebut, $nombre){   
        for ($i=$dateDebut; $i<=$dateFin ;$i++){    
        $req = $this -> queryInsert('UPDATE PLANNING SET NB_CHAMBRES_DISPO = NB_CHAMBRES_DISPO - ? where ID_HEBERG = ? and DATE = ?', [$nombre, $idHeberg, "2020-05-".$i]);
        } 
            return ($req && is_null($this->getErreur()));
    }

    public function modifAjoutNbChambres($idHeberg, $dateFin, $dateDebut, $nombre){   
        for ($i=$dateDebut; $i<=$dateFin ;$i++){    
        $req = $this -> queryInsert('UPDATE PLANNING SET NB_CHAMBRES_DISPO = NB_CHAMBRES_DISPO + ? where ID_HEBERG = ? and DATE = ?', [$nombre, $idHeberg, "2020-05-".$i]);
        } 
            return ($req && is_null($this->getErreur()));
    }

    public function supprPlanningById($idHeberg){
        for ($i=15; $i<24 ;$i++){
            $req = $this -> queryInsert('DELETE FROM PLANNING WHERE ID_HEBERG = ? and DATE = ?', array($idHeberg, "2020-05-".$i));      
        }
        return ($req && is_null($this->getErreur()));
    }

    public function verfiPlace($idHeberg, $dateFin, $dateDebut, $nombre){
        $reservation_possible = True;
        for ($i=$dateDebut; $i<$dateFin ;$i++){
            $res = $this -> queryRow('SELECT NB_CHAMBRES_DISPO FROM PLANNING WHERE ID_HEBERG = ? and DATE = ?', array($idHeberg, "2020-05-".$i));

            if($res){
                if($nombre <= $res['NB_CHAMBRES_DISPO']){
                    $reservation_possible = True;
                }
                else {
                    $reservation_possible = False;
                    return $reservation_possible;
                }
            }
        }
        return $reservation_possible;
    }

    public function HebergementIndisponible($idHeberg){
        $res = $this -> queryRow('SELECT SUM(NB_CHAMBRES_DISPO) FROM PLANNING WHERE ID_HEBERG = ?', array($idHeberg));
        if($res){
            return $res[0];
        }
        return null;
    }
}