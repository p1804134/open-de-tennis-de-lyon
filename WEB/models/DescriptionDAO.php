<?php

require_once(PATH_MODELS.'DAO.php');
require_once(PATH_ENTITY.'Description.php');
class DescriptionDAO extends DAO{
    
    public function getDescription(){
        $res = $this -> queryAll('SELECT * FROM DESCRIPTION');
        $list_description = array();
    if($res){
        
        foreach($res as $temp){
            $list_description[$temp['ID_HEBERG']] = new Description($temp['ID_HEBERG'], $temp['BAR'], $temp['RESTAURANT'], $temp['PETITDEJ'], $temp['SAUNA'], $temp['AUTRES']);
        }
    }
    return $list_description;                
}

    public function getDescriptionById($IdHeberg){
        $res = $this -> queryRow('SELECT * FROM DESCRIPTION WHERE ID_HEBERG = ?', array($IdHeberg));
        if($res){
            return new Description($res['ID_HEBERG'], $res['BAR'], $res['RESTAURANT'], $res['PETITDEJ'], $res['SAUNA'], $res['AUTRES']);
        }
        else return null;
    }

    public function changeDescription($description, $idHebergement){
        $req = $this -> queryInsert('UPDATE DESCRIPTION SET BAR = ?, RESTAURANT = ?, PETITDEJ = ?, SAUNA = ? where ID_HEBERG = ?',[$description->getBar(), $description->getRestaurant(), $description->getPetitDej(), $description->getSauna(), $idHebergement]); 
            return ($req && is_null($this->getErreur()));
    }

    public function createDescription($description){
        $req = $this -> queryInsert('INSERT INTO DESCRIPTION (BAR, RESTAURANT, PETITDEJ, SAUNA, AUTRES) VALUE(?,?,?,?,?)',[$description->getBar(), $description->getRestaurant(), $description->getPetitDej(), $description->getSauna(), $description->getAutres()]); 
            return ($req && is_null($this->getErreur()));
    }

    public function supprDescriptionById($idHeberg){
        $req = $this -> queryInsert('DELETE FROM DESCRIPTION WHERE ID_HEBERG = ?', array($idHeberg));
        return ($req && is_null($this->getErreur()));
    }

}