// set the date we're counting down to
var target_date = new Date('May, 16, 2020').getTime();
 
// variables for time units
var days, hours, minutes, seconds;
 
// get tag element
var countdown = document.getElementById('countdown');
 
// update the tag with id "countdown" every 1 second
setInterval(function () {
 
    // find the amount of "seconds" between now and target
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;
 
    // do some time calculations
    months = parseInt(seconds_left / 2628000 );
    seconds_left = seconds_left % 2628000 ;

    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;
     
    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;
     
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);
     
    // format countdown string + set tag value
    countdown.innerHTML = '<span class="h1 font-weight-bold mb-3">' + months +  ' </span> <p>Mois</p> <span class="h1 font-weight-bold mb-3" >' + days +  ' </span> <p>Jours</p><span class="h1 font-weight-bold mb-3" >' + hours
    + ' </span> <p>Heures</p> <span class="h1 font-weight-bold mb-3" >' + minutes + ' </span> <p>Minutes</p> <span class="h1 font-weight-bold mb-5" >' + seconds + ' </span> <p>Secondes</p>';
      
 
}, 1000);


