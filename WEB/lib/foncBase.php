<?php

function choixAlert($message, $nbP=null)
{
  $alert = array();
  switch($message)
  {
    case 'connexion':
      $alert['messageAlert'] = ERREUR_CONNECT_BDD;
      break;
    case 'login' :
      $alert['messageAlert'] = ERREUR_INSCRIPTION;
      break;
    case 'query' :
      $alert['messageAlert'] = ERREUR_QUERY_BDD;
      break;
    case 'url_non_valide' :
      $alert['messageAlert'] = TEXTE_PAGE_404;
      break;
    case 'not_enter_login' :
      $alert['messageAlert'] = LOGIN_NOT_ENTER;
      break;
    case 'password_invalide' :
      $alert['messageAlert'] = PASSWORD_INVALIDE;
      break;
    case 'login_incorrect' :
      $alert['messageAlert'] = LOGIN_INVALIDE;
      break;
    case 'inscription_reussie' :
      $alert['classAlert'] = 'success';
      $alert['messageAlert'] = INSCRIPTION_REUSSIE;
      break;
    case 'mail_deja_existant' : 
      $alert['messageAlert'] = MAIL_DEJA_EXISTANT;
    break;
    case 'login_pas_mail' :
      $alert['messageAlert'] = LOGIN_PAS_MAIL;
      break;
    case 'acces_interdit' : 
      $alert['messageAlert'] = ACCES_REFUSE;
      break;
    case 'mdp_renvoye' :
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = MDP_RENVOYE;
      break;
    case 'mdp_change' :
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = MDP_CHANGE;
      break;
    case 'mdp_change_expire' :
      $alert['messageAlert'] = MDP_CHANGE_EXPIRE;
      break;  
    case 'heberg_cree' :
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = HEBERG_CREE;
      break;
    case 'heberg_modifie' :
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = HEBERG_MODIFIE;
      break;
    case 'utilisateur_modifie' :
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = UTILISATEUR_MODIFIE;
      break;
    case 'utilisateur_suppr' :
      $alert['classAlert'] = 'warning';
      $alert['messageAlert'] = UTILISATEUR_SUPPR;
      break;
    case 'planning_modifie' : 
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = PLANNING_MODIFIE;
      break;
    case 'reserva_impossible' :
      $alert['messageAlert'] = RESERVA_IMPOSSIBLE;
      break;
    case 'reserva_possible' : 
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = RESERVA_POSSIBLE;
      break;
    case 'reserva_expire' :
      $alert['messageAlert'] = RESERVA_EXPIRE;
      break;
    case 'reserva_valide' : 
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = RESERVA_VALIDE;
      break;
    case 'resJoueur' : 
      $alert['classAlert'] = 'light'; 
      $alert['messageAlert'] = RESERVA_JOUEUR;
      break;
    case 'resArbitre' : 
      $alert['classAlert'] = 'light'; 
      $alert['messageAlert'] = RESERVA_ARBITRE;
      break;  
    case 'reserva_modifie' : 
      $alert['classAlert'] = 'success'; 
      $alert['messageAlert'] = RESERVA_MODIFIE;
      break;
    case 'resArbitre' : 
        $alert['classAlert'] = 'light'; 
        $alert['messageAlert'] = RESERVA_ARBITRE;
        break;
    case 'hebergement_suppr' : 
        $alert['classAlert'] = 'warning'; 
        $alert['messageAlert'] = HEBERG_SUPPR;
        break;
    case 'reservation_suppr' : 
        $alert['classAlert'] = 'warning'; 
        $alert['messageAlert'] = RESERVA_SUPPR;
        break;    
    case 'connected' :
      $alert['classAlert'] = 'success';
      $alert['messageAlert'] = MESSAGE_CONNEXION;
      break;
    case 'incorrect_password' :
      $alert['messageAlert'] = MESSAGE_INCORRECT_PASSWORD;
      break;
    case 'incorrect_login' : 
      $alert['messageAlert'] = MESSAGE_INCORRECT_LOGIN;
      break;
    case 'error_desc_add' : 
      $alert['messageAlert'] = MESSAGE_DESCRIPTION_ADD;
      break;
    case 'error_cat_add' : 
      $alert['messageAlert'] = MESSAGE_DESCRIPTION_ADD;
      break;
    case 'error_size_add' : 
      $alert['messageAlert'] = MESSAGE_SIZE_ADD;
      break;
    case 'error_extension_add' : 
      $alert['messageAlert'] = MESSAGE_EXTENSION_ADD;
      break;
    case 'error_download_add' : 
      $alert['messageAlert'] = MESSAGE_DOWNLOAD_ADD;
      break;
      
    default :
      $alert['messageAlert'] = MESSAGE_ERREUR;
  }
  return $alert;
}
