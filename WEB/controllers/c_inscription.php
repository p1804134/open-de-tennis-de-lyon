<?php 
if (isset($_POST['userId']) && isset($_POST['password']) && isset($_POST['nom']) and isset($_POST['prenom'])){
    $userId = htmlspecialchars ($_POST['userId']);
    $mdp = htmlspecialchars ($_POST['password']);
    $nom = htmlspecialchars ($_POST['nom']);
    $prenom = htmlspecialchars ($_POST['prenom']);
    $login_existant = False;
    if (!filter_var($userId, FILTER_VALIDATE_EMAIL)){ //mail valide
        $message = 'login_pas_mail';
    }
    else {
        require_once(PATH_MODELS.'UtilisateurDAO.php');
        $UtilisateurDAO = new UtilisateurDAO(DEBUG);
        $list_utilisateur = $UtilisateurDAO->getUtilisateurs();

        foreach($list_utilisateur as $utilisateur){
            if ($userId==$utilisateur->getIdUtilisateur()){
                $message = 'mail_deja_existant';
                $login_existant = True;
            }
        }
        if ($login_existant == False) {
            $utilisateur = new Utilisateur(null, $userId, password_hash($mdp, PASSWORD_DEFAULT), $nom, $prenom, "2");
            $utilisateur = $UtilisateurDAO->createUtilisateur($utilisateur);
            $_SESSION['message'] = 'inscription_reussie';
        
            $to      = $userId;
            $subject = "Confirmation d'inscription";
            $messagee = "Bienvenue ".$prenom." chez open tennis Lyon.";
            $headers = 'From: open-tennis-lyon@jokaria.fr' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $messagee, $headers);

            header('location: index.php?page=connexion');
            exit(0);
        }
    }
        if(isset($message)){
            $alert = choixAlert($message);
        }
}

require_once(PATH_VIEWS.$page.".php");