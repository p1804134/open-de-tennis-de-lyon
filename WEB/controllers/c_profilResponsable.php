<?php
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=4 ){
    $message = 'acces_interdit';
}

if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once(PATH_MODELS.'HebergementDAO.php');
$HebergementDAO = new HebergementDAO(DEBUG);
$list_hebergement = $HebergementDAO->getHebergement();

require_once(PATH_MODELS.'DescriptionDAO.php');
$DescriptionDAO = new DescriptionDAO(DEBUG);
$description = $DescriptionDAO->getDescription();

require_once(PATH_MODELS.'PlanningDAO.php');
$PlanningDAO = new PlanningDAO(DEBUG);

if (isset($_POST['type']) && isset($_POST['nbChambres']) && isset($_POST['nbEtoiles'])){
    $type = htmlspecialchars($_POST['type']);
    $nbChambre = htmlspecialchars($_POST['nbChambres']);
    $nbEtoile = htmlspecialchars($_POST['nbEtoiles']);

    if ($type!= "---" or $nbEtoile!="---" or $nbChambre!="---"){
        $list_hebergement = $HebergementDAO->getHebergementTrie($type, $nbEtoile, $nbChambre);
        $recherche = True;
    }
     //Test checkbox
    if(isset($_POST['Bar']) or isset($_POST['Restaurant']) or isset($_POST['PetitDej']) or isset($_POST['Sauna'])){
        if (isset($_POST['Bar']) and $_POST['Bar'] == "on") {
            $Bar = 1;
            echo $Bar;
        }
        if (isset($_POST['Restaurant']) and $_POST['Restaurant'] == "on") {
            $Restaurant = 1;
            echo $Restaurant;
        }
        if (isset($_POST['PetitDej']) and $_POST['PetitDej'] == "on") {
            $PetitDej = 1;
            echo $PetitDej;
        }
        if (isset($_POST['Sauna']) and $_POST['Sauna'] == "on") {
            $Sauna = 1;
            echo $Sauna;
        }
        $recherche = True;
    }
}


if(isset($message)){
    $alert = choixAlert($message);
}

require_once(PATH_VIEWS.$page.".php");