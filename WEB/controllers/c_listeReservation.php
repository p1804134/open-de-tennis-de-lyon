    <?php
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=4 ){
    $message = 'acces_interdit';
}

if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

require_once(PATH_MODELS.'HebergementDAO.php');
$HebergementDAO = new HebergementDAO(DEBUG);
$hebergement = $HebergementDAO->getHebergementById($_SESSION['IdUser']);

require_once(PATH_MODELS.'ReservationDAO.php');
$ReservationDAO = new ReservationDAO(DEBUG);
$list_reservation = $ReservationDAO->getReservation($hebergement->getIdHebergement());

require_once(PATH_MODELS.'PlanningDAO.php');
$PlanningDAO = new PlanningDAO(DEBUG);
$list_planning = $PlanningDAO->getPlanning($hebergement->getIdHebergement());

if (isset($_POST['dateSelec'])){
    $dateSelec = htmlspecialchars($_POST['dateSelec']);
}

require_once(PATH_VIEWS.$page.'.php');