<?php

if(isset($_SESSION['logged']) && $_SESSION['logged']){
    session_destroy();
    header('Location: index.php');
}