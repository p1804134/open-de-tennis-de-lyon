<?php
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=4 ){
    $message = 'acces_interdit';
}
else{

    $reservation_possible = False;

    require_once(PATH_MODELS.'HebergementDAO.php');
    $HebergementDAO = new HebergementDAO(DEBUG);
    $hebergement = $HebergementDAO->getHebergementByIdHeberg($_GET['idHeberg']);

    require_once(PATH_MODELS.'PlanningDAO.php');
    $PlanningDAO = new PlanningDAO(DEBUG);
    $list_planning = $PlanningDAO->getPlanning($hebergement->getIdHebergement());

    require_once(PATH_MODELS.'ReservationDAO.php');
    $ReservationDAO = new ReservationDAO(DEBUG);

    $resJoueur = False;
    $resArbitre = False;

    if(isset($_GET['idHeberg'])){
        $idHeberg = htmlspecialchars($_GET['idHeberg']);
        $list_reservation = $ReservationDAO-> getReservation($idHeberg);

        foreach ($list_reservation as $tempRes){
            if($tempRes->getType() == "Joueur")
                $resJoueur = True;
            else if($tempRes->getType() == "Arbitre")
                $resArbitre = True;
        }
    }

    if ($resJoueur == True)
        $message = "resJoueur";
    if ($resArbitre == True)
        $message = "resArbitre";


    if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['mail']) and isset($_POST['Type']) and isset($_POST['nombre'])){
        $nom = htmlspecialchars($_POST['nom']);
        $prenom = htmlspecialchars($_POST['prenom']);
        $mail = htmlspecialchars($_POST['mail']);
        $dateDebut = htmlspecialchars($_POST['dateDebut']);
        $dateFin = htmlspecialchars($_POST['dateFin']);
        $type = htmlspecialchars($_POST['Type']);
        $nombre = htmlspecialchars($_POST['nombre']);
        $idHeberg = htmlspecialchars($_POST['idHeberg']);

        $planning = $PlanningDAO->verfiPlace($idHeberg, $dateFin, $dateDebut, $nombre);
        if ($planning == True){
        $reservation = new Reservation (null, $idHeberg, $nom, $prenom, $mail, $type, $dateDebut, $dateFin, $nombre);
        $reservation = $ReservationDAO->ajouterReservation($reservation);

        $planningReservation = $PlanningDAO->modifNbChambres($idHeberg, $dateFin, $dateDebut, $nombre);
            $_SESSION['message'] = 'reserva_valide';
            header('location: index.php?page=profilResponsable');
            exit(0);;
        }
        else{
            $_SESSION['message'] = 'reserva_expire';
            header('location: index.php?page=profilResponsable');
            exit(0);
        }

    }

    else if (isset($_POST['dateDebut']) and isset($_POST['dateFin']) and isset($_POST['nombre'])){
        $idHeberg = htmlspecialchars($_POST['idHeberg']);
        $dateDebut = htmlspecialchars($_POST['dateDebut']);
        $dateFin = htmlspecialchars($_POST['dateFin']);
        $nombre = htmlspecialchars($_POST['nombre']);

        $duree = $dateFin - $dateDebut;
        if($dateDebut<$dateFin){

            $planning = $PlanningDAO->verfiPlace($idHeberg, $dateFin, $dateDebut, $nombre);
            if ($planning == True){
                $message = 'reserva_possible';
                $reservation_possible = True;
            }
            else{
                $message = 'reserva_impossible';
                $reservation_possible = False;
            }
        }
        else{
            $message = 'reserva_impossible';
            $reservation_possible = False;
        }
    }
}
if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.'.php');