<?php
if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=1 ){
    $message = 'acces_interdit';
}
if (isset($_GET['IdUser']) and isset($_GET['suppr'])){ //supression utlisateur
    $IdUser = htmlspecialchars($_GET['IdUser']);
    
    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->supprUtilisateurById($IdUser);

    $_SESSION['message'] = 'utilisateur_suppr';

    header('Location: index.php?page=gestionnaire');
    exit();
}

if (isset($_GET['IdHeberg']) and isset($_GET['suppr'])){ //supression hebergement
    $IdHeberg = htmlspecialchars($_GET['IdHeberg']);
    $IdUser = htmlspecialchars($_GET['IdUserHeberg']);
    
    require_once(PATH_MODELS.'HebergementDAO.php');
    $HebergementDAO = new HebergementDAO(DEBUG);
    $hebergement = $HebergementDAO->supprHebergementById($IdHeberg);

    require_once(PATH_MODELS.'DescriptionDAO.php');
    $DescriptionDAO = new DescriptionDAO(DEBUG);
    $description = $DescriptionDAO->supprDescriptionById($IdHeberg);

    require_once(PATH_MODELS.'PlanningDAO.php');
    $PlanningDAO = new PlanningDAO(DEBUG);
    $planning = $PlanningDAO->supprPlanningById($IdHeberg);

    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateurChangeType = $UtilisateurDAO->ChangeUtilisateurType2($IdUser);

    $_SESSION['message'] = 'hebergement_suppr';

    header('Location: index.php?page=gestionnaire');
    exit();
}

if (isset($_GET['NumReser']) and isset($_GET['suppr'])){ //supression reservation
    $NumReser = htmlspecialchars($_GET['NumReser']);
    
    require_once(PATH_MODELS.'ReservationDAO.php');
    $ReservationDAO = new ReservationDAO(DEBUG);

    require_once(PATH_MODELS.'PlanningDAO.php');
    $PlanningDAO = new PlanningDAO(DEBUG);

    $reservation = $ReservationDAO->getReservationbyNum($NumReser);
    $reservationModif = $ReservationDAO->supprimerReservation($NumReser);

    $planningReservation1 = $PlanningDAO->modifAjoutNbChambres($reservation->getIdHeberg(), strftime('%d',strtotime($reservation->getFin())), strftime('%d',strtotime($reservation->getDebut())), $reservation->getNbReserv());

    $_SESSION['message'] = 'reservation_suppr';

    header('Location: index.php?page=gestionnaire');
    exit();
}

if(isset($message)){
    $alert = choixAlert($message);
}

require_once(PATH_MODELS.'UtilisateurDAO.php');
$UtilisateurDAO = new UtilisateurDAO(DEBUG);
$list_utilisateur = $UtilisateurDAO->getUtilisateurs();

require_once(PATH_MODELS.'HebergementDAO.php');
$HebergementDAO = new HebergementDAO(DEBUG);
$list_hebergement = $HebergementDAO->getHebergement();

require_once(PATH_MODELS.'ReservationDAO.php');
$ReservationDAO = new ReservationDAO(DEBUG);
$list_reservation = $ReservationDAO->getReservations();

        
require_once(PATH_VIEWS.$page.".php");