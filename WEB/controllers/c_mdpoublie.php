<?php
/*function genererChaineAleatoire($longueur, $listeCar = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
 $chaine = '';
 $max = mb_strlen($listeCar, '8bit') - 1;
 for ($i = 0; $i < $longueur; ++$i) {
 $chaine .= $listeCar[random_int(0, $max)];
 }
 return $chaine;
}

if (isset($_POST['userId'])){
    $userId = htmlspecialchars ($_POST['userId']);
    $newmdp = genererChaineAleatoire(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->changeMdp($userId, password_hash($newmdp, PASSWORD_DEFAULT));

    $to = $userId;
    $subject = "Inscription";
    $messagee = "Voici votre nouveau mot de passe : ". $newmdp;
    $headers = 'From: open-tennis-lyon@jokaria.fr' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $messagee, $headers);

    $message = 'mdp_renvoye';

}
*/
if (isset($_POST['userId'])){
    $userId = htmlspecialchars ($_POST['userId']);

    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->getUtilisateurById($userId);
    
    if ($utilisateur != null){
        $utilisateurUserId = $utilisateur->getIdUtilisateur();
        $utilisateurUserMdp = $utilisateur->getMdp();

        $to = $userId;
        $subject = "Mot de passe oublié";
        $messagee = "Cliquez sur ce lien pour réinitialiser votre mot de passe : https://www.open-tennis.jokaria.fr/index.php?page=recupmdp&nu=".$utilisateurUserId."&no=".$utilisateurUserMdp."&osef";
        $headers = 'From: open-tennis-lyon@jokaria.fr' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $messagee, $headers);

        $message = 'mdp_renvoye';
    }
}

if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.".php");