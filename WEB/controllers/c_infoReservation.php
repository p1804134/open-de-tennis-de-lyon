<?php

$reservation_possible = False;



require_once(PATH_MODELS.'ReservationDAO.php');
$ReservationDAO = new ReservationDAO(DEBUG);

$resJoueur = False;
$resArbitre = False;

if(isset($_GET['numReserv'])){
    $numReserv = htmlspecialchars($_GET['numReserv']);
    $reservation = $ReservationDAO-> getReservationByNum($numReserv);

    require_once(PATH_MODELS.'HebergementDAO.php');
    $HebergementDAO = new HebergementDAO(DEBUG);
    $hebergement = $HebergementDAO->getHebergementByIdHeberg($reservation->getIdHeberg());

    require_once(PATH_MODELS.'PlanningDAO.php');
    $PlanningDAO = new PlanningDAO(DEBUG);
    $list_planning = $PlanningDAO->getPlanning($hebergement->getIdHebergement());
}

if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['mail']) and isset($_POST['nombre'])){
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $mail = htmlspecialchars($_POST['mail']);
    $dateDebut = htmlspecialchars($_POST['dateDebut']);
    $dateFin = htmlspecialchars($_POST['dateFin']);
    $nombre = htmlspecialchars($_POST['nombre']);
    $idHeberg = htmlspecialchars($_POST['idHeberg']);
    $numReserv = htmlspecialchars($_POST['numReserv']);

    if($dateDebut<$dateFin){

        $planning = $PlanningDAO->verfiPlace($idHeberg, $dateFin, $dateDebut, $nombre);
        if ($planning == True){
            $reservationModif = new Reservation ($numReserv, $idHeberg, $nom, $prenom, $mail, null, $dateDebut, $dateFin, $nombre);
            $reservationModif = $ReservationDAO->modifierReservation($reservationModif);

            $planningReservation1 = $PlanningDAO->modifAjoutNbChambres($idHeberg, strftime('%d',strtotime($reservation->getFin())), strftime('%d',strtotime($reservation->getDebut())), $reservation->getNbReserv());
            $planningReservation2 = $PlanningDAO->modifNbChambres($idHeberg, $dateFin, $dateDebut, $nombre);

            $_SESSION['message'] = 'reserva_modifie';
            header('location: index.php?page=gestionnaire');
            exit(0);;
        }
    }
    else{
        $_SESSION['message'] = 'reserva_expire';
        header('location: index.php?page=gestionnaire');
        exit(0);
    }

}


if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.'.php');