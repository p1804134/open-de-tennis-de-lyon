<?php
if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if (isset($_GET['IdUser'])){
    $IdUser = htmlspecialchars($_GET['IdUser']);
    
    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->getUtilisateurById($IdUser);
}
if (isset($_SESSION['logged']) and $_SESSION['logged']==2 || $_SESSION['logged']==3){
    $IdUser = htmlspecialchars($_SESSION['IdUser']);
    
    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->getUtilisateurById($IdUser);
}
if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['idUser']) and isset($_POST['type'])){
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $idUser = htmlspecialchars($_POST['idUser']);
    $type = htmlspecialchars($_POST['type']);
    
    $login_existant = False;

    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $list_utilisateur = $UtilisateurDAO->getUtilisateurs();

        foreach($list_utilisateur as $utilisateur2){
            if ($idUser==$utilisateur2->getIdUtilisateur() and $idUser!=$utilisateur->getIdUtilisateur()){
                $message = 'mail_deja_existant';
                $login_existant = True;
            }
        }
        if ($login_existant == False) {
        $utilisateurmodif = new Utilisateur (null, $idUser, null, $nom, $prenom, $type);
        $utilisateurmodif = $UtilisateurDAO->changeUtilisateur($utilisateurmodif, $utilisateur->getIdUtilisateur());


        $_SESSION['message'] = 'utilisateur_modifie';
        
            header('Location: index.php?page=infoUtilisateur&IdUser='.$idUser);
            exit();
        }
}
else if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['idUser'])){
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $idUser = htmlspecialchars($_POST['idUser']);

    $login_existant = False;

    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $list_utilisateur = $UtilisateurDAO->getUtilisateurs();

        foreach($list_utilisateur as $utilisateur){
            if ($idUser==$utilisateur->getIdUtilisateur() and $idUser!=$_SESSION['IdUser']){
                $message = 'mail_deja_existant';
                $login_existant = True;
            }
        }
        if ($login_existant == False) {
            
            $utilisateurmodif = new Utilisateur (null, $idUser, null, $nom, $prenom, $_SESSION['logged']);
            $utilisateurmodif = $UtilisateurDAO->changeUtilisateur($utilisateurmodif, $_SESSION['IdUser']);
            
            if ($_SESSION['logged'] == 3){
                require_once(PATH_MODELS.'HebergementDAO.php');
                $HebergementDAO = new HebergementDAO(DEBUG);
                $hebergement = $HebergementDAO->getHebergementById($_SESSION['IdUser']);

                $hebergementchange = $HebergementDAO->changeHebergementMail($idUser, $hebergement->getIdHebergement());
            }

    $_SESSION['IdUser'] = $idUser;

    $_SESSION['message'] = 'utilisateur_modifie';
    
        header('Location: index.php?page=infoUtilisateur');
        exit();
        }
}
if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.'.php');