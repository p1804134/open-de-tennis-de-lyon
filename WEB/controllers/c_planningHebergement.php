<?php
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=3 ){
    $message = 'acces_interdit';
}

if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}

require_once(PATH_MODELS.'HebergementDAO.php');
$HebergementDAO = new HebergementDAO(DEBUG);
$hebergement = $HebergementDAO->getHebergementById($_SESSION['IdUser']);

require_once(PATH_MODELS.'PlanningDAO.php');
$PlanningDAO = new PlanningDAO(DEBUG);
$list_planning = $PlanningDAO->getPlanning($hebergement->getIdHebergement());

if (isset($_POST['2020-05-15'])){
    for ($i=15; $i<24 ;$i++){ 
        $planning = $PlanningDAO->modifPlanningForHebergement($hebergement->getIdHebergement(), "2020-05-".$i, $_POST['2020-05-'.$i]);
    }
    $_SESSION['message'] = 'planning_modifie';
    header('Location: index.php?page=planningHebergement');
    exit();
}

if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.'.php');