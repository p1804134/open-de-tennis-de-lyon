<?php 
if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if(isset($_POST['userId']) && isset($_POST['password'])){
    $userId = htmlspecialchars($_POST['userId']);
    $password = htmlspecialchars($_POST['password']);

    if (!filter_var($userId, FILTER_VALIDATE_EMAIL)){
        $message = 'login_pas_mail';
    }
    else {
        $utilisateur_existant = False;
        require_once(PATH_MODELS.'UtilisateurDAO.php');
        $UtilisateurDAO = new UtilisateurDAO(DEBUG);
        $list_utilisateur = $UtilisateurDAO->getUtilisateurs();
        $message = 'login_incorrect';
        foreach($list_utilisateur as $utilisateur){
            if ($userId==$utilisateur->getIdUtilisateur() && password_verify($password,$utilisateur->getMdp())){
                $message = 'connected';
                $_SESSION['logged'] = $utilisateur->getType();
                $_SESSION['IdUser'] = $utilisateur->getIdUtilisateur();
                header('Location: index.php?page=loading');
                exit();
            }
        }    
    }
}
else if (isset($_SESSION['logged'])){
    if ( ini_get (" session . use_cookies ")) {
        $params = session_get_cookie_params ();
        setcookie ( session_name () , ’’, -1,
        $params [" path "], $params [" domain "],
        $params [" secure "], $params [" httponly "]
        );
    }
    $_SESSION = array ();// Détruit toutes les variables de session
    header('Location: index.php');
    exit();
}

if(isset($message)){
    $alert = choixAlert($message);
}

require_once(PATH_VIEWS.$page.".php");

