<?php
if (isset($_GET['nu']) and isset($_GET['no'])){
    require_once(PATH_VIEWS.$page.".php");
}

else if(isset($_POST['userId']) && isset($_POST['password']) && isset($_POST['oldMdp'])){
    $userId = htmlspecialchars ($_POST['userId']);
    $oldMdp = htmlspecialchars ($_POST['oldMdp']);
    $newmdp = htmlspecialchars ($_POST['password']);

    require_once(PATH_MODELS.'UtilisateurDAO.php');
    $UtilisateurDAO = new UtilisateurDAO(DEBUG);
    $utilisateur = $UtilisateurDAO->getUtilisateurById($userId);
    $utilisateurMdp = $utilisateur->getMdp();

    if ($oldMdp==$utilisateurMdp){

        $utilisateur = $UtilisateurDAO->changeMdp($userId, password_hash($newmdp, PASSWORD_DEFAULT));

        $to = $userId;
        $subject = "Changement mot de passe";
        $messagee = "Votre mot de passe a bien été changé";
        $headers = 'From: open-tennis-lyon@jokaria.fr' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $messagee, $headers);

        $_SESSION['message'] = 'mdp_change';

        header('Location: index.php?page=connexion');
        exit();
    }
    else {
        
        $alert = choixAlert('mdp_change_expire');
        require_once(PATH_VIEWS.$page.".php");    
    }

}
else {
    header('Location: index.php?page=connexion');
    exit();
}
