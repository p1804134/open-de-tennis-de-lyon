<?php
if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=3 ){
    $message = 'acces_interdit';
}

if(isset($_SESSION['message'])){
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
$IdUser = $_SESSION['IdUser'];
require_once(PATH_MODELS.'HebergementDAO.php');
$HebergementDAO = new HebergementDAO(DEBUG);
$hebergement = $HebergementDAO->getHebergementById($IdUser);

require_once(PATH_MODELS.'DescriptionDAO.php');
$DescriptionDAO = new DescriptionDAO(DEBUG);
$description = $DescriptionDAO->getDescriptionById($hebergement->getIdHebergement());

require_once(PATH_MODELS.'PlanningDAO.php');
$PlanningDAO = new PlanningDAO(DEBUG);
$list_planning = $PlanningDAO->getPlanning($hebergement->getIdHebergement());
$hebergDispo = $PlanningDAO->HebergementIndisponible($hebergement->getIdHebergement());


if(isset($message)){
    $alert = choixAlert($message);
}

require_once(PATH_VIEWS.$page.".php");