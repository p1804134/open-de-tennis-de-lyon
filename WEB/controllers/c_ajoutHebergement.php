<?php

function genererChaineAleatoire($longueur, $listeCar = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') //Création d'un mot de passe aléatoire
{
 $chaine = '';
 $max = mb_strlen($listeCar, '8bit') - 1;
 for ($i = 0; $i < $longueur; ++$i) {
 $chaine .= $listeCar[random_int(0, $max)];
 }
 return $chaine;
}
    $newmdp = genererChaineAleatoire(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

if (!isset($_SESSION['logged'])){
    header('Location: index.php');
    exit();
}
if (isset($_SESSION['logged']) && $_SESSION['logged']!=3 and $_SESSION['logged']!=4 and $_SESSION['logged']!=1){
    $message = 'acces_interdit';
}

if (isset($_SESSION['logged']) && $_SESSION['logged']==4){ // Utilisateur responsable tournoi
    if (isset($_POST['nom']) && isset($_POST['idGerant'])){
        $nom = htmlspecialchars($_POST['nom']);
        $idGerant = htmlspecialchars($_POST['idGerant']);

        require_once(PATH_MODELS.'HebergementDAO.php'); //Creation de l'hebergement
        $HebergementDAO = new HebergementDAO(DEBUG);
        $hebergementcreate = new Hebergement (null, $nom, null, $idGerant, null, null, null);
        $hebergementcreate = $HebergementDAO->createHebergement($hebergementcreate);

        require_once(PATH_MODELS.'DescriptionDAO.php'); //Creation de la description de l'hebergement
        $DescriptionDAO = new DescriptionDAO(DEBUG);
        $descriptiontcreate = new Description (null, 0, 0, 0, 0, 0);
        $descriptiontcreate = $DescriptionDAO->createDescription($descriptiontcreate);

        $hebergement = $HebergementDAO->getHebergementById($idGerant);

        require_once(PATH_MODELS.'PlanningDAO.php'); //Creation de du planning
        $PlanningDAO = new PlanningDAO(DEBUG);
        $planning = $PlanningDAO->createPlanningForHebergement($hebergement->getIdHebergement());
        

        require_once(PATH_MODELS.'UtilisateurDAO.php');
        $UtilisateurDAO = new UtilisateurDAO(DEBUG);
        $list_utilisateur = $UtilisateurDAO->getUtilisateurs();

        foreach($list_utilisateur as $utilisateur){
            if ($idGerant==$utilisateur->getIdUtilisateur()){
                $utilisateurChangeType = $UtilisateurDAO->ChangeUtilisateurType3($idGerant);
                $login_existant = True;
            }
        }
        if ($login_existant == False) {
            $utilisateur = new Utilisateur(null,$idGerant, password_hash($newmdp, PASSWORD_DEFAULT), null, null, "3");
            $utilisateur = $UtilisateurDAO->createUtilisateur($utilisateur);

            $to      = $idGerant;
            $subject = "Création d'un compte gérant hebergeur";
            $messagee = "Un compte hebergeur a été crée avec votre adresse mail sur https://www.open-tennis.jokaria.fr/, n'oubliez pas de mettre à jour vos informations personnelles, ainsi que les informations de votre hébergement.
Voici votre nouveau mot de passe : ". $newmdp;
            $headers = 'From: open-tennis-lyon@jokaria.fr' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $messagee, $headers);
        }
        $_SESSION['message'] = 'heberg_cree';

        header('Location: index.php?page=profilResponsable');
        exit();

    }
}
if (isset($_SESSION['logged']) && $_SESSION['logged']==3){
    
    
        $IdUser = $_SESSION['IdUser'];
        require_once(PATH_MODELS.'HebergementDAO.php');
        $HebergementDAO = new HebergementDAO(DEBUG);
        $hebergement = $HebergementDAO->getHebergementById($IdUser);
        require_once(PATH_MODELS.'DescriptionDAO.php');
        $DescriptionDAO = new DescriptionDAO(DEBUG);
        $description = $DescriptionDAO->getDescriptionById($hebergement->getIdHebergement());

    if (isset($_POST['nom']) && isset($_POST['type']) && isset($_POST['lieux']) && isset($_POST['nbEtoiles']) &&  isset($_POST['nbChambres'])) {
        $nom = htmlspecialchars($_POST['nom']);
        $type = htmlspecialchars($_POST['type']);
        $lieux = htmlspecialchars($_POST['lieux']);
        $nbEtoiles = ($_POST['nbEtoiles']);
        $nbChambres = ($_POST['nbChambres']);

        require_once(PATH_MODELS.'HebergementDAO.php');
        $HebergementDAO = new HebergementDAO(DEBUG);

        $hebergementchange = new Hebergement (null, $nom, $type, null, $lieux, $nbEtoiles, $nbChambres);
        $hebergementchange = $HebergementDAO->changeHebergement($hebergementchange, $hebergement->getIdHebergement());

        $bar = 0;$restaurant = 0;$petitDej = 0;$sauna = 0;

        if (isset($_POST['Bar']))
            $bar = 1;
        if (isset($_POST['Restaurant']))
            $restaurant = 1;
        if (isset($_POST['PetitDej']))
            $petitDej = 1;
        if (isset($_POST['Sauna']))
            $sauna = 1;

        $descriptiontchange = new Description (null, $bar, $restaurant, $petitDej, $sauna, null);
        $descriptiontchange = $DescriptionDAO->changeDescription($descriptiontchange, $hebergement->getIdHebergement());
        
        $_SESSION['message'] = 'heberg_modifie';

        header('Location: index.php?page=profilhebergeur');
        exit();

    }

}

if (isset($_SESSION['logged']) && $_SESSION['logged']==1){
    
    
    $IdUser = $_GET['IdUser'];
    require_once(PATH_MODELS.'HebergementDAO.php');
    $HebergementDAO = new HebergementDAO(DEBUG);
    $hebergement = $HebergementDAO->getHebergementById($IdUser);
    require_once(PATH_MODELS.'DescriptionDAO.php');
    $DescriptionDAO = new DescriptionDAO(DEBUG);
    $description = $DescriptionDAO->getDescriptionById($hebergement->getIdHebergement());

if (isset($_POST['nom']) && isset($_POST['type']) && isset($_POST['lieux']) && isset($_POST['nbEtoiles']) &&  isset($_POST['nbChambres'])) {
    $nom = htmlspecialchars($_POST['nom']);
    $type = htmlspecialchars($_POST['type']);
    $lieux = htmlspecialchars($_POST['lieux']);
    $nbEtoiles = ($_POST['nbEtoiles']);
    $nbChambres = ($_POST['nbChambres']);

    require_once(PATH_MODELS.'HebergementDAO.php');
    $HebergementDAO = new HebergementDAO(DEBUG);

    $hebergementchange = new Hebergement (null, $nom, $type, null, $lieux, $nbEtoiles, $nbChambres);
    $hebergementchange = $HebergementDAO->changeHebergement($hebergementchange, $hebergement->getIdHebergement());

    $bar = 0;$restaurant = 0;$petitDej = 0;$sauna = 0;

    if (isset($_POST['Bar']))
        $bar = 1;
    if (isset($_POST['Restaurant']))
        $restaurant = 1;
    if (isset($_POST['PetitDej']))
        $petitDej = 1;
    if (isset($_POST['Sauna']))
        $sauna = 1;

    $descriptiontchange = new Description (null, $bar, $restaurant, $petitDej, $sauna, null);
    $descriptiontchange = $DescriptionDAO->changeDescription($descriptiontchange, $hebergement->getIdHebergement());
    
    $_SESSION['message'] = 'heberg_modifie';

    header('Location: index.php?page=gestionnaire');
    exit();

}

}
if(isset($message)){
    $alert = choixAlert($message);
}
require_once(PATH_VIEWS.$page.".php");

