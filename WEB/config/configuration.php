<?php
/* Module de PHP
 * Paramètres de configuration du site
 *
 * Copyright 2017, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
 
const DEBUG = true; // production : false; dev : true

// Accès base de données
const BD_HOST = 'iutdoua-web.univ-lyon1.fr';
const BD_DBNAME = 'p1805931';
const BD_USER = 'p1805931';
const BD_PWD = '368907';

// Langue du site
const LANG ='FR-fr';

// Taille max photo
const FILE_SIZE = 100000;

//dossiers racines du site
define('PATH_CONTROLLERS','./controllers/c_');
define('PATH_ASSETS','./assets/');
define('PATH_LIB','./lib/');
define('PATH_MODELS','./models/');
define('PATH_VIEWS','./views/v_');
define('PATH_TEXTES','./languages/');
define('PATH_ENTITY','./entities/');

//sous dossiers
define('PATH_CSS', PATH_ASSETS.'css/');
define('PATH_JS', PATH_ASSETS.'scripts/');
define('PATH_IMAGES', PATH_ASSETS.'images/');
define('PATH_SCRIPTS',PATH_ASSETS.'scripts/');
//define("PATH_GALERIE",'galerie/');
//define('PATH_LOG','log/');

//fichiers
//define('LOG_BDD','logbdd.txt');
define('PATH_MENU', PATH_VIEWS.'menu.php');

define('PATH_CONNEXION_BD', PATH_MODELS.'Connexion.php');

setlocale(LC_ALL, 'fr_FR.UTF8', 'fr_FR','fr','fr','fra','fr_FR@euro');
