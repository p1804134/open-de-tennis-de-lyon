<?php

class Utilisateur {

    private $_numUtilisateur;
    private $_idUtilisateur;
    private $_mdp;
    private $_nom;
    private $_prenom;
    private $_type;

    public function __construct($numUtilisateur, $idUtilisateur, $mdp, $nom, $prenom, $type) {
        $this->_numUtilisateur = $numUtilisateur;
        $this->_idUtilisateur = $idUtilisateur;
        $this->_mdp = $mdp;
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_type = $type;
    }

    public function getNumUtilisateur(){
        return $this->_numUtilisateur;
    }

    public function getIdUtilisateur(){
        return $this->_idUtilisateur;
    }
    public function getMdp(){
        return $this->_mdp;
    }
    public function getNom(){
        return $this->_nom;
    }
    public function getPrenom(){
        return $this->_prenom;
    }
    public function getType(){
        return $this->_type;
    }
}