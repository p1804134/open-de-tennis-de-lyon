<?php

class Description {

    private $_idHebergement;
    private $_bar;
    private $_restaurant;
    private $_petitdej;
    private $_sauna;
    private $_autres;

    public function __construct($idHebergement, $bar, $restaurant, $petitdej, $sauna, $autres) {
        $this->_bar = $bar;
        $this->_restaurant = $restaurant;
        $this->_petitdej = $petitdej;
        $this->_sauna = $sauna;
        $this->_autres = $autres;
    }

    public function getIdHebergement(){
        return $this->_idHebergement;
    }
    public function getBar(){
        return $this->_bar;
    }
    public function getRestaurant(){
        return $this->_restaurant;
    }
    public function getPetitDej(){
        return $this->_petitdej;
    }
    public function getSauna(){
        return $this->_sauna;
    }
    public function getAutres(){
        return $this->_autres;
    }
}