<?php

class Planning {

    private $_idHeberg;
    private $_date;
    private $_nbChambresDispo;

    public function __construct($idHeberg, $date, $nbChambresDispo) {
        $this->_idHeberg = $idHeberg;
        $this->_date = $date;
        $this->_nbChambresDispo = $nbChambresDispo;
    }

    public function getIdHeberg(){
        return $this->_idHeberg;
    }
    public function getDate(){
        return $this->_date;
    }
    public function getNbChambresDispo(){
        return $this->_nbChambresDispo;
    }

}