<?php

class Reservation {

    private $_numReserv;
    private $_idHeberg;
    private $_nom;
    private $_prenom;
    private $_mail;
    private $_type;
    private $_debut;
    private $_fin;
    private $_nbReserv;

    public function __construct($numReserv, $idHeberg, $nom, $prenom, $mail, $type, $debut, $fin, $nbReserv) {
        $this->_numReserv = $numReserv;
        $this->_idHeberg = $idHeberg;
        $this->_nom = $nom;
        $this->_prenom = $prenom;
        $this->_mail = $mail;
        $this->_type = $type;
        $this->_debut = $debut;
        $this->_fin = $fin;
        $this->_nbReserv = $nbReserv;
    }

    public function getNumReserv(){
        return $this->_numReserv;
    }
    public function getIdHeberg(){
        return $this->_idHeberg;
    }
    public function getNom(){
        return $this->_nom;
    }
    public function getPrenom(){
        return $this->_prenom;
    }
    public function getMail(){
        return $this->_mail;
    }
    public function getType(){
        return $this->_type;
    }
    public function getDebut(){
        return $this->_debut;
    }
    public function getFin(){
        return $this->_fin;
    }
    public function getNbReserv(){
        return $this->_nbReserv;
    }

}