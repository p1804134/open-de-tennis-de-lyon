<?php

class Hebergement {

    private $_idHebergement;
    private $_nom;
    private $_type;
    private $_idGerant;
    private $_lieux;
    private $_nbEtoiles;
    private $_nbChambres;

    public function __construct($idHebergement, $nom, $type, $idGerant, $lieux, $nbEtoiles, $nbChambres) {
        $this->_idHebergement = $idHebergement;
        $this->_nom = $nom;
        $this->_type = $type;
        $this->_idGerant = $idGerant;
        $this->_lieux = $lieux;
        $this->_nbEtoiles = $nbEtoiles;
        $this ->_nbChambres = $nbChambres;
    }

    public function getIdHebergement(){
        return $this->_idHebergement;
    }
    public function getNom(){
        return $this->_nom;
    }
    public function getType(){
        return $this->_type;
    }
    public function getIdGerant(){
        return $this->_idGerant;
    }
    public function getLieux(){
        return $this->_lieux;
    }
    public function getNbEtoiles(){
        return $this->_nbEtoiles;
    }
    public function getNbChambres(){
        return $this->_nbChambres;
    }
}