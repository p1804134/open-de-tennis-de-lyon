<?php require_once(PATH_VIEWS."header.php");?>

<div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image2"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
            <?php require_once(PATH_VIEWS.'alert.php');?>
              <h3 class="login-heading mb-4">Inscription</h3>
              <form method="post">
                <div class="form-label-group">
                  <p>Veuillez renseigner une adresse mail valide</p>
                  <input type="email" name="userId" id="exampleInputEmail1" class="form-control" placeholder="Votre adresse mail" required autofocus>
                  <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais votre courriel avec qui que ce soit d'autre.</small>
                </div>
                <div class="form-label-group">
                  <p>Veuillez renseigner un mot de passe</p>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Votre mot de passe">
                </div>
                <div class="form-label-group">
                  <p>Veuillez renseigner votre nom</p>
                  <input type="text" name="nom" class="form-control" id="exampleInputPassword1" placeholder="Votre nom">
                </div>
                <div class="form-label-group">
                  <p>Veuillez renseigner votre prénom</p>
                  <input type="text" name="prenom" class="form-control" id="exampleInputPassword1" placeholder="Votre prénom">
                </div>
                <hr style="margin: 21px">
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">S'inscrire</button>
                <div class="text-center">
                  <p style="margin-top: 15px">Retour à <a class="" href="index.php">l'accueil </a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>