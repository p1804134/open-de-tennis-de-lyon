<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==4 ){?> <!--verification de l'user connecté-->

<div class="container">
  <br/>
	<div class="row justify-content-center">
    <div class="col-12 col-md-12 col-lg-12">
              <div class="card-body row no-gutters align-items-center">
                  <div class="col-auto">
                      <i class="fas fa-search h4 text-body"></i>
                  </div>
                  <!--end of col-->
                  <div class="col">
                      <input class="form-control form-control-lg form-control-borderless" onkeyup="recherche()" type="search" id="barre-recherche" placeholder="Recherche">
                  </div>
                  <!--end of col-->
              </div>
      </div>
      <!--end of col-->
  </div>
  
  <form class="" method="post">
  <div class="row">
    <div class="col-lg-12 mx-auto py-3">
      <!-- Accordion -->
      <div id="accordionExample" class="accordion shadow">

        <!-- Accordion item 1 -->
        <div class="card">
          <div id="headingOne" class="card-header bg-white shadow-sm border-0">
            <h6 class="mb-0 font-weight-bold"><a href="#" data-toggle="collapse" data-target="#collapseOne"  aria-controls="collapseOne" class="d-block position-relative text-dark text-uppercase collapsible-link py-2">Recherche avancée</a></h6>
          </div>
          <div id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample" class="collapse <?php if (isset($recherche)) echo "show"?>">
            <div class="card-body p-3">
            <div class="tab-pane fade show active py-2 px-2" id="home">
              <div class="row col-md-12">
              <div class="form-group-group col-lg-6">
                <p style="margin-bottom: 8px">Type d'hébergement</p>
                <select class="form-control" id="exampleSelect1" name="type" style="margin-bottom: 8px">
                  <option>---</option>
                  <option <?php if (isset($type) and $type == "Autre") echo "selected"?>>Autre</option>
                  <option <?php if (isset($type) and $type == "Hotel") echo "selected"?>>Hotel</option>
                  <option <?php if (isset($type) and $type == "Auberge") echo "selected"?>>Auberge</option>
                </select>
              </div>
              <div class="form-group-group col-lg-6">
                <p style="margin-bottom: 8px">Nombres d'étoiles</p>
                <select class="form-control" id="exampleSelect1" name="nbEtoiles" style="margin-bottom: 8px">
                  <option>---</option>
                  <option <?php if (isset($nbEtoile) and $nbEtoile == "1") echo "selected"?>>1</option>
                  <option <?php if (isset($nbEtoile) and $nbEtoile == "2") echo "selected"?>>2</option>
                  <option <?php if (isset($nbEtoile) and $nbEtoile == "3") echo "selected"?>>3</option>
                  <option <?php if (isset($nbEtoile) and $nbEtoile == "4") echo "selected"?>>4</option>
                  <option <?php if (isset($nbEtoile) and $nbEtoile == "5") echo "selected"?>>5</option>
                </select>
              </div>
              <div class="form-group-group col-lg-6">
                <p style="margin-bottom: 8px">Nombres de chambres</p>
                <select class="form-control" id="exampleSelect1" name="nbChambres" style="margin-bottom: 8px">
                  <option>---</option>
                  <option value ="10" <?php if (isset($nbChambre) and $nbChambre == "10") echo "selected"?>>10 +</option>
                  <option value ="50" <?php if (isset($nbChambre) and $nbChambre == "50") echo "selected"?>>50 +</option>
                  <option value ="100" <?php if (isset($nbChambre) and $nbChambre == "100") echo "selected"?>>100 +</option>
                  <option value ="150" <?php if (isset($nbChambre) and $nbChambre == "150") echo "selected"?>>150 +</option>
                  <option value ="250" <?php if (isset($nbChambre) and $nbChambre == "250") echo "selected"?>>250 +</option>
                </select>
              </div>
              <div class="form-group-group col-lg-12">
                <button type="submit" class="btn btn-primary btn-md mt-3" style="width:100%">Lancer la recherche</button>
              </div>
              <!--
              <div class="form-label-group col-md-12">
                  <p>Description</p>
                  <div class="row col-lg-12">
                  <div class="custom-control custom-checkbox col-lg-3">
                      <input type="checkbox" name = "Bar" class="custom-control-input" id="customCheck1" <?php if (isset($Bar) and $Bar == "1") echo "checked"?>>
                      <label class="custom-control-label" for="customCheck1">Bar</label>
                  </div>
                  <div class="custom-control custom-checkbox col-lg-3">
                      <input type="checkbox" name = "Restaurant" class="custom-control-input" id="customCheck2" <?php if (isset($Restaurant) and $Restaurant == "1") echo "checked"?>>
                      <label class="custom-control-label" for="customCheck2">Restaurant</label>
                  </div>
                  <div class="custom-control custom-checkbox col-lg-3">
                      <input type="checkbox" name = "PetitDej" class="custom-control-input" id="customCheck3" <?php if (isset($PetitDej) and $PetitDej == "1") echo "checked"?>>
                      <label class="custom-control-label" for="customCheck3">Petit-déjeuner</label>
                  </div>
                  <div class="custom-control custom-checkbox col-lg-3">
                      <input type="checkbox" name = "Sauna" class="custom-control-input" id="customCheck4" <?php if (isset($Sauna) and $Sauna == "1") echo "checked"?>>
                      <label class="custom-control-label" for="customCheck4">Sauna</label>
                  </div>
                  </div>
              </div>
              -->
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>

<div class="container">
  <div class="row py-5" >
  <?php foreach ($list_hebergement as $temp) { 
          $temp2  = $description[$temp->getIdHebergement()];
          $temp3 = $hebergDispo = $PlanningDAO->HebergementIndisponible($temp->getIdHebergement());?>
          
            <div class="col-lg-12 mx-auto hebs" data-name="<?= $temp->getNom() ?>">
                <div class="card shadow mb-4">
                    <div class="card-body p-5" >
                      <div class="row col-md-12">
                        <div class="card border-primary md-2 mr-4">
  
                          <div class="card-body" style="">
                            <i class="fas fa-<?php if ($temp->getType() == "Hotel") echo ("building"); else if ($temp->getType() == "Auberge") echo ("home") ?> fa-5x px-3 py-3"></i>
                          </div>
                        </div>
  
                        <div class="container col-md-3 py-3">
                          <p class="mb-5" style="font-size: 22px"><i class="fas fa-city mr-2"> :</i> <?php echo ($temp->getNom())?></p>
                          <p class="" style="font-size: 22px"><i class="fas fa-<?php if ($temp->getType() == "Hotel") echo ("building"); else if ($temp->getType() == "Auberge") echo ("home") ?> mr-2"> :</i> <?php echo ($temp->getType())?></p>
                        </div>
                        <div class="container col-md-3 py-3" style="">
                          <p class="mb-5" style="font-size: 22px"><i class="fas fa-bed mr-2"> :</i> <?php echo ($temp->getNbChambres())?></p>
                          <p class="" style="font-size: 22px"><i class="fas fa-star mr-2"> :</i> <?php echo ($temp->getNbEtoiles())?></p>
                        </div>
                        <div class="container col-md-3 py-3" style="text-align:center;">
                          <button <?php if ($temp3==0) echo "disabled"?> type="button" class="btn btn-outline-<?php if ($temp3!=0) echo "primary"; else echo "secondary"?> btn-lg" style="width:75%;margin-bottom: 12%;" onclick='document.location.href="index.php?page=reservation&idHeberg=<?php echo $temp->getIdHebergement()?>";'>Réserver</button>
                          <button type="button" class="btn btn-outline-primary btn-lg" style="width:75%" data-toggle="collapse" data-target="#<?php echo ($temp->getIdHebergement())?>">+ Infos</button>
                        </div>

                        <div id="<?php echo ($temp->getIdHebergement())?>" class="collapse col-md-12" style="padding: 0px">
                        <div class="card border-primary mt-4" style="text-align:center;">
                          <div class="card-header " style="font-weight: bold;font-size: 22px">Informations</div>

                          <div class="card-body" style="text-align: justify">
                          <h6 class="pb-2"><b>Location</b> : <?php echo ($temp->getLieux())?></h6>
                          <div class="table-responsive">
                            <table id="example" style="text-align:center;" class="table table-striped table-bordered ">
                              <thead>
                                <tr>
                                  <th>Services</th>
                                  <th>Disponible</th>
                                  <th>Indisponible</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Bar</td>
                                  <td><?php if ($temp2->getBar() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                                  <td><?php if ($temp2->getBar() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                                </tr>
                                <tr>
                                  <td>Restaurant</td>
                                  <td><?php if ($temp2->getRestaurant() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                                  <td><?php if ($temp2->getRestaurant() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                                </tr>
                                <tr>
                                  <td>Petit-déjeuner</td>
                                  <td><?php if ($temp2->getPetitDej() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                                  <td><?php if ($temp2->getPetitDej() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                                </tr>
                                <tr>
                                  <td>Sauna</td>
                                  <td><?php if ($temp2->getSauna() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                                  <td><?php if ($temp2->getSauna() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          </div>
                        </div>   
                        </div> 

                      </div>
                    </div>
                  </div>
              </div>
        <?php } ?>
  </div>
</div>
<?php } ?><!--verification de l'user connecté-->

<script>
  function recherche(){
    var barre = document.getElementById('barre-recherche').value.toLowerCase();
    var hebs = document.getElementsByClassName('hebs');
    for(i=0; i<hebs.length; i++){
      if(hebs[i].getAttribute('data-name').toLowerCase().indexOf(barre) >= 0){
        hebs[i].style.display = "block"
      }else{
        hebs[i].style.display = "none";
      }
    }
  }
</script>