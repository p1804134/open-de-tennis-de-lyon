<?php require_once(PATH_VIEWS."header.php");?>

<div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
            <?php require_once(PATH_VIEWS.'alert.php');?>
              <h3 class="login-heading mb-4">Entrez votre nouveau mot de passe</h3>
              <form method="post" action="index.php?page=recupmdp">
                

                <div class="form-label-group">
                  <input type="password" name="password" id="inputPassword" class="form-control"  required>
                  <label for="inputPassword">Mot de passe</label>
                </div>
                    <input type="hidden" name="userId" id="inputEmail" value="<?php if(isset($_GET['nu'])) echo $_GET['nu'] ?>">
                    <input type="hidden" name="oldMdp" id="inputPassword" value="<?php if(isset($_GET['no'])) echo $_GET['no'] ?>">
                
                <hr style="margin: 21px">
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Changer le mot de passe</button>
            
              </form>
              <div class="text-center">
                  <p style="margin-top: 15px">Retour à <a class="" href="index.php">l'accueil </a></p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
