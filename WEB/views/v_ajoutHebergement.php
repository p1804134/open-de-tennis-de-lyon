<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==3 or $_SESSION['logged']==4 or $_SESSION['logged']==1){?> <!--verification de l'user connecté-->

<div class="container py-5">
<form method="post" action="">
    <ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#home">Informations</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#details">Détails</a>
    </li>

    </ul>
    <div id="myTabContent" class="tab-content">
    <div class="tab-pane fade show active py-4 px-4" id="home">
        <div class="form-label-group">
            <p>Nom</p>
            <input type="text" name="nom" <?php if (isset($hebergement) and $hebergement != null) echo ('value="'.$hebergement->getNom().'"')?> id="" class="form-control" placeholder="Nom de l'hebergement" required autofocus>
        </div>
        <div class="form-label-group">
            <p>Mail gérant</p>
            <input type="mail" <?php if (isset($hebergement) and $hebergement != null) echo 'value="'.$hebergement->getIdGerant().'"'?> name="idGerant" id="" class="form-control" placeholder="Adresse mail" required autofocus <?php if($_SESSION['logged']==3) echo "disabled" ?>>
        </div>
    

        <div class="form-label-group">
            <p>Adresse</p>
            <input type="text" name="lieux" <?php if (isset($hebergement) and $hebergement != null) echo 'value="'.$hebergement->getLieux().'"'?> id="" class="form-control" placeholder="Votre adresse" required autofocus <?php if($_SESSION['logged']==4) echo "disabled" ?>>
        </div>
        <div class="form-group-group">
            <p>Type</p>
            <select class="form-control" id="exampleSelect1" name="type" <?php if($_SESSION['logged']==4) echo "disabled" ?>>
                <?php if (!isset($hebergement)) echo "<option>---</option>"?>
                <option <?php if (isset($hebergement) and $hebergement->getType()=="Hotel") echo "selected"?> value="Hotel" >Hotel</option>
                <option <?php if (isset($hebergement) and $hebergement->getType()=="Auberge") echo "selected"?> value="Auberge">Auberge de jeunesse</option>
                <option <?php if (isset($hebergement) and $hebergement->getType()=="Autres") echo "selected"?> value="Autres">Autre</option>
            </select>
        </div>
        </div>
    <div class="tab-pane fade py-4 px-4" id="details">
    <div class="form-group-group">
      <p style="margin-bottom: 8px">Nombres d'étoiles</p>
      <select class="form-control" id="exampleSelect1" name="nbEtoiles" style="margin-bottom: 8px" <?php if($_SESSION['logged']==4) echo "disabled" ?>>
        <?php if (!isset($hebergement)) echo "<option>---</option>"?>
        <option <?php if (isset($hebergement) and $hebergement->getNbEtoiles()==1) echo "selected"?>>1</option>
        <option <?php if (isset($hebergement) and $hebergement->getNbEtoiles()==2) echo "selected"?>>2</option>
        <option <?php if (isset($hebergement) and $hebergement->getNbEtoiles()==3) echo "selected"?>>3</option>
        <option <?php if (isset($hebergement) and $hebergement->getNbEtoiles()==4) echo "selected"?>>4</option>
        <option <?php if (isset($hebergement) and $hebergement->getNbEtoiles()==5) echo "selected"?>>5</option>
      </select>
    </div>
    <div class="form-label-group">
        <p>Nombres de chambres</p>
        <input type="number" name="nbChambres" <?php if (isset($hebergement) and $hebergement != null) echo "value=".$hebergement->getNbChambres().""?> id="" class="form-control"  required autofocus <?php if($_SESSION['logged']==4) echo "disabled" ?>>
    </div>
    <div class="form-label-group">
        <p>Description</p>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name = "Bar" class="custom-control-input" id="customCheck1" <?php if (isset($description) and $description->getBar()==1) echo "checked"?> <?php if($_SESSION['logged']==4) echo "disabled" ?>> <!-- disabled="" pour désactiver-->
            <label class="custom-control-label" for="customCheck1">Bar</label>
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name = "Restaurant" class="custom-control-input" id="customCheck2" <?php if (isset($description) and $description->getRestaurant()==1) echo "checked"?> <?php if($_SESSION['logged']==4) echo "disabled" ?>>
            <label class="custom-control-label" for="customCheck2">Restaurant</label>
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name = "PetitDej" class="custom-control-input" id="customCheck3" <?php if (isset($description) and $description->getPetitDej()==1) echo "checked"?> <?php if($_SESSION['logged']==4) echo "disabled" ?>>
            <label class="custom-control-label" for="customCheck3">Petit-déjeuner</label>
        </div>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name = "Sauna" class="custom-control-input" id="customCheck4" <?php if (isset($description) and $description->getSauna()==1) echo "checked"?> <?php if($_SESSION['logged']==4) echo "disabled" ?>>
            <label class="custom-control-label" for="customCheck4">Sauna</label>
        </div>
    </div>
    <div class="form-label-group">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customCheck5" <?php if($_SESSION['logged']==4) echo "disabled" ?>>
            <label class="custom-control-label" for="customCheck5" style="margin-bottom: 8px">Autres </label>
        </div>
        <input type="text" name="" id="" class="form-control"  placeholder="" autofocus <?php if($_SESSION['logged']==4) echo "disabled" ?>>

    </div>
    </div>

    </div>
    <button style="margin-top: 30px"class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Envoyer</button>

</form>
<?php if ($_SESSION['logged']==1) echo ('<button type="button" class="btn btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" style="margin-top: 30px" data-toggle="modal" data-target="#exampleModalCenter">Supprimer l\'hébergement</button>');?>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Suppression Hebergement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Confirmer la suppression
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-login text-uppercase font-weight-bold mb-2" onclick='document.location.href="index.php?page=gestionnaire&suppr=true&IdUserHeberg=<?php echo $hebergement->getIdGerant()?>&IdHeberg=<?php echo $hebergement->getIdHebergement()?>";'>Confirmer</button>
                        <button type="button" class="btn btn-secondary btn-login text-uppercase font-weight-bold mb-2" data-dismiss="modal">Annuler</button>
                      </div>
                    </div>
                  </div>
                </div>
</div>
</div>

<?php } ?><!--verification de l'user connecté-->
