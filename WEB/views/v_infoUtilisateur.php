<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==1 || $_SESSION['logged']==2 || $_SESSION['logged']==3){?> <!--verification de l'user connecté-->

<div class="container py-3">
<form method="post">
    <div class="form-label-group">
        <p>Nom</p>
        <input type="text" name="nom" <?php if (isset($utilisateur)) echo 'value="'.$utilisateur->getNom().'"'?> id="" class="form-control"  required autofocus>
    </div>
    
    <div class="form-label-group">
        <p>Prénom</p>
        <input type="text" name="prenom" <?php if (isset($utilisateur)) echo 'value="'.$utilisateur->getPrenom(); echo'"'?> id="" class="form-control"  required autofocus>
    </div>
    <div class="form-label-group">
        <p>Mail utilisateur</p>
        <input type="mail" <?php if (isset($utilisateur)) echo "value=".$utilisateur->getIdUtilisateur().""?> name="idUser" id="" class="form-control" required autofocus>
    </div>
  <?php if ($_SESSION['logged']==1) {?>
    <div class="form-group-group">
      <p>Type</p>
      <select class="form-control" id="exampleSelect1" name="type" <?php if($_SESSION['logged']==4) echo "disabled" ?>>
        <option value = "1" <?php if (isset($utilisateur) and $utilisateur->getType()==1) echo "selected"?>>Admin</option>
        <option value = "2" <?php if (isset($utilisateur) and $utilisateur->getType()==2) echo "selected"?>>Visiteur</option>
        <option value = "3" <?php if (isset($utilisateur) and $utilisateur->getType()==3) echo "selected"?>>Gerant hebergement</option>
        <option value = "4" <?php if (isset($utilisateur) and $utilisateur->getType()==4) echo "selected"?>>Responsable Tournoi</option>
        <option value = "5" <?php if (isset($utilisateur) and $utilisateur->getType()==5) echo "selected"?>>Joueur</option>
      </select>
    </div>
  <?php } ?>
    
    
    <button style="margin-top: 30px"class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Modifier les informations</button>
</form>

<?php if ($utilisateur->getType()!=1 and $_SESSION['logged']==1) echo ('<button type="button" class="btn btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" style="margin-top: 30px" data-toggle="modal" data-target="#exampleModalCenter">Supprimer l\'utilisateur</button>');?>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Suppression Utilisateur</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Confirmer la suppression
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-login text-uppercase font-weight-bold mb-2" onclick='document.location.href="index.php?page=gestionnaire&suppr=true&IdUser=<?php echo $utilisateur->getIdUtilisateur()?>";'>Confirmer</button>
                        <button type="button" class="btn btn-secondary btn-login text-uppercase font-weight-bold mb-2" data-dismiss="modal">Annuler</button>
                      </div>
                    </div>
                  </div>
                </div>
</div>
<?php } ?><!--verification de l'user connecté-->
