<div class="container-fluid" id="sectionPlanning">
  <div class="row no-gutter">
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items py-3">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-10 mx-auto">
                <h1 class="login-heading mb-1">Planning</h1>
                <small><a href="index.php#sectionAccueil">Revenir à l'accueil</a></small>
                <hr class="md-4">
                <div class="py-4">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <!-- Countdown 3-->
                            <div class="rounded bg-gradient-3 text-white shadow p-1 text-center mb-5">
                                <div id="countdown" class="countdown py-3" ></div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="d-none d-md-flex col-md-4 col-lg-6 login d-flex align-items py-3" style="margin-top: 1vh">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators" style="margin-bottom: 35px">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="./assets/images/planning1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="./assets/images/planning2.jpg" alt="Second slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
      </div>
    </div>
  </div>
</div>
<script src="<?= PATH_JS ?>appJs.js"></script>