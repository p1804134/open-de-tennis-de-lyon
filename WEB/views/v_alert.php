<?php

if(isset($alert))
{

?>
	<div class="alert alert-<?= isset($alert['classAlert']) ? $alert['classAlert'] : 'danger' ?>" style="margin-top: 25px">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4><strong><?= $alert['messageAlert'] ?></strong></h4>
		
	</div>
<?php
}
