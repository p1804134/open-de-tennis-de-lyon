<?php require_once(PATH_VIEWS."header.php");?>

<div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
            <?php require_once(PATH_VIEWS.'alert.php');?>
              <h3 class="login-heading mb-4">Connexion</h3>
              <form method="post">
                <div class="form-label-group">
                  <input type="email" name="userId" id="inputEmail" class="form-control"  placeholder="Adresse mail"required autofocus> 
                </div>
                <div class="form-label-group">
                  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required>
                </div>
                <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" class="custom-control-input" id="customCheck1">
                  <p style="margin-top: 15px">Mot de passe <a class="" href="index.php?page=mdpoublie">oublié </a>?</p>
                </div>
                <hr style="margin: 21px">
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Se connecter</button>
                <div class="text-center">
                  <p style="margin-top: 15px">Si vous ne possédez pas de compte,  <a class="" href="index.php?page=inscription">inscrivez-vous</a> maintenant !</p>
                  <hr style="width: 65%">
                  <p style="margin-top: 15px">Retour à <a class="" href="index.php">l'accueil </a></p>
                </div>
                  
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
