<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.php?page=<?php if($_SESSION['logged'] == 4) echo"profilResponsable"; else if ($_SESSION['logged'] == 3) echo"profilhebergeur"; else if ($_SESSION['logged'] == 2) echo"profilUtilisateur"; else if ($_SESSION['logged'] == 1) echo"gestionnaire";?>">| <?php if($_SESSION['logged'] == 4) echo"Responsable"; else if ($_SESSION['logged'] == 3) echo"Hebergeur"; else if ($_SESSION['logged'] == 2) echo"Utilisateur"; else if ($_SESSION['logged'] == 1) echo"Gestion";?> |</a> <!-- index.php?page=gestionnaire quand admin  -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
    <?php if($_SESSION['logged'] == 2 || $_SESSION['logged'] == 3){ ?>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=infoUtilisateur">Modifier mon profil</a>        
      </li>
    <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=ajoutHebergement"><?php if($_SESSION['logged'] == 4) echo"Ajout hebergement"; else if ($_SESSION['logged'] == 3) echo"Modifier hebergement";?></a>        
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=planningHebergement"><?php if($_SESSION['logged'] == 3) echo"Planning des chambres";?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?page=listeReservation"><?php if($_SESSION['logged'] == 3) echo"Liste des réservations";?></a>
      </li>
    </ul>
      <span class="navbar-nav mr-auto"></span>
      <a class="nav-item nav-link" href="index.php?page=connexion" style="color: white; font-style: bold;">Déconnexion</a>
  </div>
</nav>