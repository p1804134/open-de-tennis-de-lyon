<header id="sectionAccueil">
  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="./assets/video/clipDrone.mp4" type="video/mp4">
  </video>
  <div class="container h-100">
    <div class="d-flex text-center h-100">
      <div class="my-auto w-100 text-white" style="background-color: rgb(192,192,192,0.4); padding: 20px; border-radius: 50px" >
        
        <h1 class="display-3">Open de Tennis de Lyon | 2019</h1>
        <hr>
        <div class="row-" style="">
          <button type="button" class="btn btn-primary btn-lg col-md-2" style="margin-right: 50px; margin-bottom: 25px;" onclick='document.location.href="index.php#sectionPresentation";'>Présentation</button>
          <button type="button" class="btn btn-primary btn-lg col-md-2" style="margin-right: 50px; margin-bottom: 25px;" onclick='document.location.href="index.php#sectionPlanning";'>Planning</button>
          <button type="button" class="btn btn-primary btn-lg col-md-2" style="margin-right: 50px; margin-bottom: 25px;" onclick='document.location.href="index.php?page=loading";'>Billetterie</button>
          <button type="button" class="btn btn-primary btn-lg col-md-2" style="margin-bottom: 25px;" onclick='document.location.href="index.php?page=connexion";'><?php if(isset($_SESSION['logged'])) echo "Deconnexion"; else echo "Connexion"?></button>
        </div>
        
      </div>
    </div>
  </div>
</header>




