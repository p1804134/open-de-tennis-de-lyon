<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==4 ){?> <!--verification de l'user connecté-->
<div class="container py-5">
<form method="post" action="">
    <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Informations</a>
      </li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane fade show active py-4" id="home">
         <input type="hidden" name="idHeberg" id="" class="form-control" value="<?php echo $_GET['idHeberg']?>" required autofocus>
      <?php if ($reservation_possible == True) { ?>
            <div class="form-label-group">
                <p>Nom</p>
                <input type="text" name="nom" id="" class="form-control" placeholder="Nom" required autofocus>
            </div>
            <div class="form-label-group">
                <p>Prénom</p>
                <input type="text" name="prenom" id="" class="form-control" placeholder="Prénom" required autofocus>
            </div>
            <div class="form-label-group">
                <p>Adresse mail</p>
                <input type="text" name="mail" id="" class="form-control" placeholder="Mail" required autofocus>
            </div>
            <?php } ?>
            <div class="form-label-group">
                <p>Date de début</p>
                <select class="form-control" id="exampleSelect1" name="dateDebut" style="margin-bottom: 8px" >
                <?php foreach ($list_planning as $temp1) { ?>
                  <option <?php if(isset($dateDebut) and strftime('%d',strtotime($temp1->getDate()))==$dateDebut) echo "selected"; else if($reservation_possible == True) echo "disabled"?> value="<?php echo strftime('%d',strtotime($temp1->getDate()))?>"><?php echo strftime('%A %d %B %Y',strtotime($temp1->getDate()))?></option>
                <?php } ?>  
                </select>
            </div>
            <div class="form-label-group">
                <p>Date de fin</p>
                <select class="form-control" id="exampleSelect2" name="dateFin" style="margin-bottom: 8px" >        
                <?php foreach ($list_planning as $temp1) { ?>
                  <option <?php if(isset($dateFin) and strftime('%d',strtotime($temp1->getDate()))==$dateFin) echo "selected"; else if($reservation_possible == True) echo "disabled"?> value="<?php echo strftime('%d',strtotime($temp1->getDate()))?>"><?php echo strftime('%A %d %B %Y',strtotime($temp1->getDate()))?></option>
                <?php } ?>
                </select>
            </div>
            <?php if ($reservation_possible == True) { ?>
            <div class="form-label-group">
                <p style="margin-bottom: 8px">Type</p>
                <select class="form-control" id="exampleSelect1" name="Type" style="margin-bottom: 8px" >
                    <option <?php if ($resArbitre==True) echo "disabled"?>>Joueur</option>
                    <option <?php if ($resJoueur==True) echo "disabled"?>>Arbitre</option>
                </select>
            </div>
            <div class="form-label-group">
                <p>Si le joueur fait parti d'une équipe, veuillez indiquer le nombre de chambres <b>total</b></p>
                <input class="form-control" style="text-align:center;" name="nombre" type="number" value=<?php if(isset($nombre)) echo $nombre?>>
            </div>
            <?php } ?>
            <?php if ($reservation_possible == False) { ?>
            <div class="form-label-group">
                <p>Nombre de chambres</p>
                <input class="form-control" required style="text-align:center;" name="nombre" type="number" value=<?php if(isset($nombre)) echo $nombre?>>
            </div>
                <?php } ?>
            <button style="margin-top: 30px"class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Réserver</button>
      </div>
</form>
</div>
<?php } ?>