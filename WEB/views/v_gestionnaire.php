<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==1 ){?> <!--verification de l'user connecté-->

<div class="container py-5">
    <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Tableau utilisateurs</a>
      </li>

      <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#details">Tableau hebergements</a>
      </li>

      <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#reservations">Tableau réservations</a>
      </li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane fade py-4 px-4 show active" id="home">
        <div class="row">
          <div class="col-lg-12 mx-auto" >
            <div class="card rounded shadow border-0" >
              <div class="card-body p-5 bg-white rounded">
                <div class="table-responsive">
                  <table id="example" style="width:100%" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Adresse mail</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Type</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($list_utilisateur as $temp) { ?>
                      <tr>
                        <td onclick='document.location.href="index.php?page=infoUtilisateur&IdUser=<?php echo $temp->getIdUtilisateur()?>";'><?php echo $temp->getIdUtilisateur()?></td>
                        <td onclick='document.location.href="index.php?page=infoUtilisateur&IdUser=<?php echo $temp->getIdUtilisateur()?>";'><?php echo $temp->getNom()?></td>
                        <td onclick='document.location.href="index.php?page=infoUtilisateur&IdUser=<?php echo $temp->getIdUtilisateur()?>";'><?php echo $temp->getPrenom()?></td>
                        <td onclick='document.location.href="index.php?page=infoUtilisateur&IdUser=<?php echo $temp->getIdUtilisateur()?>";'><?php if ($temp->getType()==1) echo "Admin"; else if ($temp->getType()==2) echo "Simple visiteur"; else if($temp->getType()==3) echo "Gérant hébergement"; else if($temp->getType()==4) echo "Responsable tournoi"; ?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
          
      <div class="tab-pane fade py-4 px-4" id="details">
        <div class="row">
            <div class="col-lg-12 mx-auto" >
              <div class="card rounded shadow border-0" >
                <div class="card-body p-5 bg-white rounded">
                  <div class="table-responsive">
                    <table id="tableau" style="width:100%" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Nom hébergement</th>
                          <th>Type</th>
                          <th>Mail gérant</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($list_hebergement as $temp) { ?>
                        <tr>
                          <td onclick='document.location.href="index.php?page=ajoutHebergement&IdUser=<?php echo $temp->getIdGerant()?>";'><?php echo $temp->getNom()?></td>
                          <td onclick='document.location.href="index.php?page=ajoutHebergement&IdUser=<?php echo $temp->getIdGerant()?>";'><?php echo $temp->getType()?></td>
                          <td onclick='document.location.href="index.php?page=ajoutHebergement&IdUser=<?php echo $temp->getIdGerant()?>";'><?php echo $temp->getIdGerant()?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="tab-pane fade py-4 px-4" id="reservations">
        <div class="row">
            <div class="col-lg-12 mx-auto" >
              <div class="card rounded shadow border-0" >
                <div class="card-body p-5 bg-white rounded">
                  <div class="table-responsive">
                    <table id="reserv" style="width:100%" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                        <th>Numéro réservation</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Mail</th>
                        <th>Type</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Nombre de réservation</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($list_reservation as $temp) {?>
                      <tr>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getNumReserv()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getNom()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getPrenom()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getMail()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getType()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getDebut()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getFin()?></td>
                        <td onclick='document.location.href="index.php?page=infoReservation&numReserv=<?php echo $temp->getNumReserv()?>";'><?php echo $temp->getNbReserv()?></td>
                      </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(function() {
        $(document).ready(function() {
        $('#example').DataTable();
        });
    });

    $(function() {
        $(document).ready(function() {
        $('#tableau').DataTable();
        });
    });

    
    $(function() {
        $(document).ready(function() {
        $('#reserv').DataTable();
        });
    });
</script>
<?php } ?><!--verification de l'user connecté-->