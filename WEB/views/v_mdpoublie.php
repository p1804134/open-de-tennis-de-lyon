<?php require_once(PATH_VIEWS."header.php");?>

<div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
            <?php require_once(PATH_VIEWS.'alert.php');?>
              <h3 class="login-heading mb-4">Mot de passe oublié</h3>
              <form method="post">
                <div class="form-label-group">
                  <input type="email" name="userId" id="inputEmail" class="form-control"  required autofocus>
                  <label for="inputEmail">Adresse mail</label>
                </div>     
                <hr style="margin: 21px">
                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Récupérer mon mot de passe</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
