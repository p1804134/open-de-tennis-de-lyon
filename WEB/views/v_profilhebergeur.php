<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==3 ){?> <!--verification de l'user connecté-->

<div class="container">
  <div class="row">
    <div class=" mx-auto py-5" style="width: 100%">
      <!-- Accordion -->
      <div id="accordionExample" class="accordion shadow">

        <!-- Accordion item 1 -->
        <div class="card">
          <div id="headingOne" class="card-header bg-white shadow-sm border-0">
            <nav class="navbar navbar-expand-lg">
            <h6 class="mb-0 font-weight-bold"><a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block position-relative text-dark text-uppercase collapsible-link py-2">Présentation de l'hébergement</a></h6>
                <div class="collapse navbar-collapse" id="navbarColor01">
                  <span class="navbar-nav mr-auto"></span>
                  <p class="form-inline my-2 my-lg-0 bold" style="color: <?php if ($hebergDispo!=0) echo "green"; else echo "red"?>;font-weight:bold;"><?php if ($hebergDispo!=0) echo "Disponible"; else echo "Indisponible"?></p>
                </div>
            </nav>
          </div>
          <div id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample" class="collapse show py-4 px-4">
            <div class="container">
              <div class="row col-md-12">
                <div class="card border-primary md-2 mr-4" style="text-align:center;">
                  <div class="card-header" style="">
                  <?php for ($i=0; $i< 5; $i++){
                    if ($i<$hebergement->getNbEtoiles())
                    echo ('<i class="fas fa-star px-2"></i>');
                    else 
                    echo ('<i class="far fa-star px-2"></i>');
                  } ?>
                  <!--affichage etoiles -->

                  </div>
                  <div class="card-body" style="">
                    <i class="fas fa-<?php if ($hebergement->getType() == "Hotel") echo ("building"); else if ($hebergement->getType() == "Auberge") echo ("home") ?> fa-5x px-3 py-3"></i><!--affichage icone -->
                  </div>
                </div>
                <div class="conatainer col-md-9 py-3">
                  <p class="" style="font-size: 22px"><i class="fas fa-city mr-2"> :</i> <?php echo ($hebergement->getNom())?></p>
                  <p class="" style="font-size: 22px"><i class="fas fa-<?php if ($hebergement->getType() == "Hotel") echo ("building"); else if ($hebergement->getType() == "Auberge") echo ("home") ?> mr-2"> :</i> <?php echo ($hebergement->getType())?></p>
                  <p class="" style="font-size: 22px"><i class="fas fa-map-marker-alt mr-2"> :</i> <?php echo ($hebergement->getLieux())?> </p>
                  <p class="mb-0" style="font-size: 22px"><i class="fas fa-envelope mr-2"> :</i> <?php echo ($hebergement->getIdGerant())?></p>
                </div>
              </div>
              <div class="card border-primary mt-4" style="text-align:center;">
                <div class="card-header " style="font-weight: bold;font-size: 22px">Informations</div>

                <div class="card-body" style="text-align: justify">
                <div class="table-responsive">
                  <table id="example" style="text-align:center;" class="table table-striped table-bordered ">
                    <thead>
                      <tr>
                        <th>Services</th>
                        <th>Disponible</th>
                        <th>Indisponible</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Bar</td>
                        <td><?php if ($description->getBar() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                        <td><?php if ($description->getBar() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                      </tr>
                      <tr>
                        <td>Restaurant</td>
                        <td><?php if ($description->getRestaurant() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                        <td><?php if ($description->getRestaurant() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                      </tr>
                      <tr>
                        <td>Petit-déjeuner</td>
                        <td><?php if ($description->getPetitDej() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                        <td><?php if ($description->getPetitDej() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                      </tr>
                      <tr>
                        <td>Sauna</td>
                        <td><?php if ($description->getSauna() == "1") echo ('<i class="fas fa-check px-2"></i>'); ?></td>
                        <td><?php if ($description->getSauna() == "0") echo ('<i class="fas fa-times px-2"></i>'); ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>

              <div class="card border-primary mt-4" style="text-align:center;">
                <div class="card-header " style="font-weight: bold;font-size: 22px">Planning</div>

                <div class="card-body" style="text-align: justify">
                <div class="table-responsive">
                  <table id="example" style="text-align:center;" class="table table-striped table-bordered ">
                <thead>
                    <tr>
                    <th style="width:50%">Date</th>
                    <th  style="width:50%">Nombre de chambres disponibles</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($list_planning as $temp) { ?>
                    <tr>
                        <td><?php echo strftime('%A %d %B %Y',strtotime($temp->getDate()))?></td>
                        <td><?php echo $temp->getNbChambresDispo()?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
                </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<?php } ?><!--verification de l'user connecté-->