<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==3 ){?> <!--verification de l'user connecté-->

<div class="container py-5">
    <h1>Veuillez chosir un jour : </h1>                   <!-- Job -->
        <div class="input-group col-lg-12 mb-4 py-2">
        <div class="row col-md-12">
            <div class="input-group-prepend">
                <span class="input-group-text bg-white px-4 border-md border-right-0">
                <i class="fas fa-calendar"></i>
                </span>
            </div>
            <form method="post">
              <select id="job" name="dateSelec" class="form-control custom-select bg-white border-left-0 border-md">
                <option value=0>Tous les jours</option>
                <?php foreach ($list_planning as $temp1) { ?>
                  <option <?php if(isset($dateSelec) and $temp1->getDate()==$dateSelec) echo "selected"?> value="<?php echo $temp1->getDate()?>"><?php echo strftime('%A %d %B %Y',strtotime($temp1->getDate()))?></option>
                <?php } ?>
              </select>
            </div>
            <button type="submit" class="mt-3 col-md-3">Valider</button>
            <form>
        </div>

        <div class="col-lg-12 mx-auto" >
            <div class="card rounded shadow border-0" >
              <div class="card-body p-5 bg-white rounded">
                <div class="table-responsive">
                  <table id="example" style="width:100%" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Numéro réservation</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Mail</th>
                        <th>Type</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Nombre de réservation</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($dateSelec) and $dateSelec!=0){
                      foreach ($list_reservation as $temp) { 
                        if (strftime('%d',strtotime($dateSelec))>=strftime('%d',strtotime($temp->getDebut())) and strftime('%d',strtotime($dateSelec))<=strftime('%d',strtotime($temp->getFin()))) {?>
                      <tr>
                        <td><?php echo $temp->getNumReserv()?></td>
                        <td><?php echo $temp->getNom()?></td>
                        <td><?php echo $temp->getPrenom()?></td>
                        <td><?php echo $temp->getMail()?></td>
                        <td><?php echo $temp->getType()?></td>
                        <td><?php echo $temp->getDebut()?></td>
                        <td><?php echo $temp->getFin()?></td>
                        <td><?php echo $temp->getNbReserv()?></td>
                      </tr>
                      <?php } } }
                      else {
                        foreach ($list_reservation as $temp) {?>
                      <tr>
                        <td><?php echo $temp->getNumReserv()?></td>
                        <td><?php echo $temp->getNom()?></td>
                        <td><?php echo $temp->getPrenom()?></td>
                        <td><?php echo $temp->getMail()?></td>
                        <td><?php echo $temp->getType()?></td>
                        <td><?php echo $temp->getDebut()?></td>
                        <td><?php echo $temp->getFin()?></td>
                        <td><?php echo $temp->getNbReserv()?></td>
                      </tr>
                        <?php } } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
</div>



<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(function() {
        $(document).ready(function() {
        $('#example').DataTable();
        });
    });

    $(function() {
        $(document).ready(function() {
        $('#tableau').DataTable();
        });
    });
</script>

<?php } ?><!--verification de l'user connecté-->
