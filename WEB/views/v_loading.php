<?php require_once(PATH_VIEWS."header.php");?>

<div class="loadingText">
    <h1 class="col-md-12">Chargement en cours</h1>
</div>

<div class="loading">
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>
      <div class="obj"></div>

</div>

<script>
    <?php
        if ($_SESSION['logged'] == 1){
            
    ?>
    function myFunction() {
        setTimeout(function(){ window.location.href="index.php?page=gestionnaire"; }, 1200);
    }
    window.onload = myFunction;
    <?php
        } 
        else if ($_SESSION['logged'] == 2){

        
    ?>
    function myFunction() {
        setTimeout(function(){ window.location.href="index.php?page=profilUtilisateur"; }, 1200);
    }
    window.onload = myFunction; 
    <?php
        } 
        else if ($_SESSION['logged'] == 3){

        
    ?>
    function myFunction() {
        setTimeout(function(){ window.location.href="index.php?page=profilhebergeur"; }, 1200);
    }
    window.onload = myFunction;
    <?php
        } 
        else if ($_SESSION['logged'] == 4){

        
    ?>
    function myFunction() {
        setTimeout(function(){ window.location.href="index.php?page=profilResponsable"; }, 1200);
    }
    window.onload = myFunction;
    <?php
        }
    ?>
</script>