<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>

<div class="container py-5">
    <div class="card">
          <div id="headingOne" class="card-header bg-white shadow-sm border-0">
            <nav class="navbar navbar-expand-lg">
            <h6 class="mb-0 font-weight-bold"><a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block position-relative text-dark text-uppercase collapsible-link py-2">Planning</a></h6>
            </nav>
    </div>
    <div class="card-body" style="text-align: justify;">
        <div class="table-responsive">
        <form method = "post">
            <table id="example" style="text-align:center;" class="table table-striped table-bordered ">
                <thead>
                    <tr>
                    <th style="width:50%">Date</th>
                    <th  style="width:50%">Nombre de chambres disponibles</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($list_planning as $temp) { ?>
                    <tr>
                        <td><?php echo strftime('%A %d %B %Y',strtotime($temp->getDate()))?></td>
                        <td><input style="text-align:center;" name = <?php echo $temp->getDate()?> type="text" value="<?php echo $temp->getNbChambresDispo()?>"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <button class="btn btn-lg btn-primary" type="submit" style="width:100%">Valider</button>
            
        <form>
        </div>
    </div>
</div>