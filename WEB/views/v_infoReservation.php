<?php require_once(PATH_VIEWS.'header.php');?>
<?php require_once(PATH_VIEWS.'menuGestion.php');?>
<!--  Zone message d'alerte -->
<?php require_once(PATH_VIEWS.'alert.php');?>
<?php if (isset($_SESSION['logged']) && $_SESSION['logged']==1 ){?> <!--verification de l'user connecté-->

<div class="container py-5">
<form method="post" action="">
    <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Informations</a>
      </li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane fade show active py-4" id="home">
         <input type="hidden" name="idHeberg" id="" class="form-control" value="<?php echo $reservation->getIdHeberg()?>" required autofocus>
         <input type="hidden" name="numReserv" id="" class="form-control" value="<?php echo $reservation->getNumReserv()?>" required autofocus>

            <div class="form-label-group">
                <p>Nom</p>
                <input type="text" name="nom" id="" class="form-control" placeholder="Nom" required autofocus value=<?php echo $reservation->getNom()?>>
            </div>
            <div class="form-label-group">
                <p>Prénom</p>
                <input type="text" name="prenom" id="" class="form-control" placeholder="Prénom" required autofocus value=<?php echo $reservation->getPrenom()?>>
            </div>
            <div class="form-label-group">
                <p>Adresse mail</p>
                <input type="text" name="mail" id="" class="form-control" placeholder="Mail" required autofocus value=<?php echo $reservation->getMail()?>>
            </div>
            <div class="form-label-group">
                <p>Date de début</p>
                <select class="form-control" id="exampleSelect1" name="dateDebut" style="margin-bottom: 8px" >
                <?php foreach ($list_planning as $temp1) { ?>
                  <option <?php if($temp1->getDate()==$reservation->getDebut()) echo "selected"?> value="<?php echo strftime('%d',strtotime($temp1->getDate()))?>"><?php echo strftime('%A %d %B %Y',strtotime($temp1->getDate()))?></option>
                <?php } ?>  
                </select>
            </div>
            <div class="form-label-group">
                <p>Date de fin</p>
                <select class="form-control" id="exampleSelect2" name="dateFin" style="margin-bottom: 8px" >        
                <?php foreach ($list_planning as $temp1) { ?>
                  <option <?php if($temp1->getDate()==$reservation->getFin()) echo "selected"?> value="<?php echo strftime('%d',strtotime($temp1->getDate()))?>"><?php echo strftime('%A %d %B %Y',strtotime($temp1->getDate()))?></option>
                <?php } ?>
                </select>
            </div>
            
            <div class="form-label-group">
                <p style="margin-bottom: 8px">Type</p>
                <select class="form-control" id="exampleSelect1" name="Type" style="margin-bottom: 8px" disabled>
                    <option <?php if ($resArbitre==True) echo "disabled"?>>Joueur</option>
                    <option <?php if ($resJoueur==True) echo "disabled"?>>Arbitre</option>
                </select>
            </div>
            <div class="form-label-group">
                <p>Si le joueur fait parti d'une équipe, veuillez indiquer le nombre de personne <b>en plus</b></p>
                <input class="form-control" style="text-align:center;" name="nombre" type="number" value=<?php echo $reservation->getNbReserv()?>>
            </div>
            
            <button style="margin-top: 30px"class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Envoyer</button>
      
</form>
<?php if ($_SESSION['logged']==1) echo ('<button type="button" class="btn btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" style="margin-top: 30px" data-toggle="modal" data-target="#exampleModalCenter">Supprimer la réservation</button>');?>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Suppression Reservation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Confirmer la suppression
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-login text-uppercase font-weight-bold mb-2" onclick='document.location.href="index.php?page=gestionnaire&suppr=true&NumReser=<?php echo $reservation->getNumReserv()?>";'>Confirmer</button>
                        <button type="button" class="btn btn-secondary btn-login text-uppercase font-weight-bold mb-2" data-dismiss="modal">Annuler</button>
                      </div>
                    </div>
                  </div>
                </div>
        </div>
</div>
<?php } ?><!--verification de l'user connecté-->