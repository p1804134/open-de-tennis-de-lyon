<?php
//isoler ici dans des constantes les textes affichés sur le site
define('LOGO', 'Logo de la compagnie'); // Affiché si image non trouvée

define('MENU_ACCUEIL','Accueil');
define('MENU_PARTENAIRE','Accès partenaire');
define('CONNEXION','Connexion');
define('DECONNEXION','Deconnexion');
define('TITRE_PAGE_ACCUEIL','Accueil');
define('TITRE_PAGE_HELLO','Résultat');
define('TITRE', 'Open de Tennis de Lyon');
define('TEXTE_PAGE_404','Attention, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR',"Une erreur s'est produite");
define('MESSAGE_CONNEXION',"Vous êtes connecté(e)");
define('MESSAGE_INCORRECT_PASSWORD',"Mot de passe incorrect");
define('MESSAGE_INCORRECT_LOGIN',"Login incorrect");
define('ERREUR_CONNECT_BDD', 'Connexion à la BD impossible.');
define('ERREUR_INSCRIPTION', 'Le login est inconnu!');
define('ERREUR_QUERY_BDD', 'Problème avec la requete.');
define('LOGIN_NOT_ENTER', 'Veuillez saisir le login!');


//Page connexion/inscription
define('PASSWORD_INVALIDE', 'Mot de passe incorrect');
define('LOGIN_INVALIDE', 'Login incorrect');
define('INSCRIPTION_REUSSIE', 'Inscription validée, un mail de confirmation a été envoyé');
define('MAIL_DEJA_EXISTANT', 'Le mail entré est déjà utilisé');
define('LOGIN_PAS_MAIL', 'Entrez une adresse mail valide !');
define('ACCES_REFUSE', 'Vous n\'avez pas accés à cette page !');
define('MDP_RENVOYE', 'Si votre adresse mail est valide, vous recevrez un e-mail de récupération de votre mot de passe');
define('MDP_CHANGE', 'Votre mot de passe a été changé avec succés');
define('MDP_CHANGE_EXPIRE', 'Expiré');

//hebergement
define('HEBERG_CREE', 'L\'hébergement a été crée, un email a été envoyé au gérant de l\'hébergement');
define('HEBERG_MODIFIE', 'Les informations concernant votre hébergement ont été mis à jour');
define('PLANNING_MODIFIE', 'Le planning a été mis à jour');
define('HEBERG_SUPPR', 'L\'hébergement a bien été supprimé');

//user
define('UTILISATEUR_MODIFIE', 'Les informations de cet utilisateur ont été mis à jour');
define('UTILISATEUR_SUPPR', 'L\'utilisateur a bien été supprimé');

//reservation
define('RESERVA_IMPOSSIBLE', 'La reservation est impossible');
define('RESERVA_POSSIBLE', 'La reservation est possible');
define('RESERVA_EXPIRE', 'Le temps de réservation a expiré');
define('RESERVA_VALIDE', 'La réservation a été effectué');
define('RESERVA_JOUEUR', 'Un ou plusieurs joueurs sont hébérgés dans cet hébergement');
define('RESERVA_ARBITRE', 'Un ou plusieurs arbitres sont hébérgés dans cet hébergement');
define('RESERVA_MODIFIE', 'La réservation a bien été modifié');
define('RESERVA_SUPPR', 'La réservation a bien été supprimé');