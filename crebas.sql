/*==============================================================*/
/* Nom de SGBD :  Sybase SQL Anywhere 12                        */
/* Date de cr�ation :  04/12/2019 14:47:39                      */
/*==============================================================*/


drop index if exists UTILISATEUR.UTILISATEUR_PK;

drop table if exists UTILISATEUR;

/*==============================================================*/
/* Table : UTILISATEUR                                          */
/*==============================================================*/
create table UTILISATEUR 
(
   ID_UTILISATEUR       varchar(255)                   not null,
   MDP                  varchar(256)                   null,
   NOM                  varchar(255)                   null,
   PRENOM               varchar(255)                   null,
   TYPE                 integer                        not null,
   constraint PK_UTILISATEUR primary key (ID_UTILISATEUR)
);

/*==============================================================*/
/* Index : UTILISATEUR_PK                                       */
/*==============================================================*/
create unique index UTILISATEUR_PK on UTILISATEUR (
ID_UTILISATEUR ASC
);

